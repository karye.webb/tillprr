# Table of contents

## Introduktion

* [Introduktion](README.md)

## Spelprogrammering

* [RPG Maker VX Ace Lite](spelprogrammering/rpg-maker/README.md)
  * [Skapa kartor](spelprogrammering/rpg-maker/kartor.md)
  * [Dialoger](spelprogrammering/rpg-maker/dialoger.md)
  * [Events](spelprogrammering/rpg-maker/events.md)
  * [Scenarior](spelprogrammering/rpg-maker/scenarior.md)
* [Pico-8](spelprogrammering/Pico-8/README.md)
  * [Rita i Pico-8](spelprogrammering/Pico-8/rita-pico8.md)
  * [Rymd shooter](spelprogrammering/Pico-8/space.md)
  * [Racingbana](spelprogrammering/Pico-8/racingbana.md)

## Pico programmering
* [Vad är Pico?](praktisk-programmering/vad-aer-pico.md)
* [Utvecklingsmiljö](praktisk-programmering/utvecklingsmiljoe.md)
* [MicroPython](praktisk-programmering/micropython.md)
* [Programmera Pico](praktisk-programmering/ansluta-komponenter/README.md)
  * [Prova inbyggda led](praktisk-programmering/ansluta-komponenter/prova-taenda-led.md)
  * [Prova temperatursensor](praktisk-programmering/ansluta-komponenter/prova-tempsensor.md)
  * [Prova extern led](praktisk-programmering/ansluta-komponenter/prova-extern-led.md)
  * [Prova summer](praktisk-programmering/ansluta-komponenter/prova-summer.md)
  * [Prova potentiometer](praktisk-programmering/ansluta-komponenter/prova-potentiometer.md)
  * [Prova knapp](praktisk-programmering/ansluta-komponenter/prova-knapp.md)
  * [Prova 7-segmentsdisplay](praktisk-programmering/ansluta-komponenter/prova-7-segment.md)
* [Labb trafikljus](praktisk-programmering/trafikljus.md)
* [Labb reaktionsspel](praktisk-programmering/reaktionsspel.md)

## Kalkylblad skripting

* [Google kalkylblad](kalkylblad-skripting/google-kalkylblad.md)

## Linux skripting

* [Installera Linux Ubuntu](linux-skripting/installera-linux-ubuntu.md)
* [Vad är Linux?](linux-skripting/linux.md)
* [Terminalen](linux-skripting/README.md)
  * [Bash - intro](linux-skripting/output-input/README.md)
    * [Skriva ut](linux-skripting/output-input/ovning-1a.md)
    * [Läsa in](linux-skripting/output-input/ovning-1b.md)
    * [Argument](linux-skripting/output-input/ovning-1c.md)
  * [Navigera i filsystemet](linux-skripting/ovning-2.md)
  * [Skapa mappar och filer](linux-skripting/ovning-3.md)
  * [Kopiera och flytta filer](linux-skripting/ovning-4.md)
  * [Använda loopar](linux-skripting/ovning-5.md)
  * [Hantera textfiler](linux-skripting/ovning-6.md)
  * [Enkla villkor](linux-skripting/ovning-7.md)
  * [Argument och loopar](linux-skripting/ovning-8.md)