# Övningsuppgifter i Bash

Här går vi igenom grunderna i Bash-programmering genom att skapa enkla skript. Varje steg innehåller en kort beskrivning, en uppgift och en utmaning. Vi kan använda Windows Subsystem for Linux (WSL) eller en annan terminal för att köra skripten.

## Bash-argument

När du kör ett bash-skript kan du skicka med argument från kommandoraden. Dessa argument kan sedan användas inuti skriptet.

> **Tips om bash-argument:**  
> - När du kör ett skript med argument skrivs de efter skriptnamnet, t.ex. `./script.sh arg1 arg2`.  
> - Inom skriptet refererar du till det första argumentet med `$1`, det andra med `$2` och så vidare.  
> - Variabeln `$@` innehåller alla argument.

---

### Steg 41: Visa det första argumentet
**Mål:** Skapa ett skript som skriver ut det första argumentet du anger.

1. Skapa filen `arg41.sh`:
   ```bash
   nano arg41.sh
   ```
2. Skriv in följande kod:
   ```bash
   #!/bin/bash
   echo "Det första argumentet är: $1"
   ```
3. Spara, avsluta, gör skriptet körbart och testa med:
   ```bash
   chmod +x arg41.sh
   ./arg41.sh Hej
   ```
   
**Förklaring:**  
- `$1` representerar det första argumentet som skickas till skriptet.

**Utmaning:** Ändra utskriften så att den till exempel säger: "Du skrev: $1" istället.

---

### Steg 42: Visa två argument
**Mål:** Skapa ett skript som tar emot två argument och skriver ut dem på separata rader.

1. Skapa filen `arg42.sh`:
   ```bash
   nano arg42.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Första argumentet: $1"
   echo "Andra argumentet: $2"
   ```
3. Spara, avsluta, gör körbart och testa med:
   ```bash
   chmod +x arg42.sh
   ./arg42.sh Äpple Banan
   ```

**Förklaring:**  
- `$1` och `$2` används för att referera till det första respektive andra argumentet.

**Utmaning:** Prova att ändra skriptet så att det skriver ut ett meddelande på tredje raden, t.ex. "Fantastiskt val!".

---

### Steg 43: Personlig hälsning med namn
**Mål:** Skapa ett skript som hälsar på användaren med det namn som anges som argument.

1. Skapa filen `arg43.sh`:
   ```bash
   nano arg43.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Hej, $1! Välkommen till Bash!"
   ```
3. Spara, avsluta, gör körbart och testa med:
   ```bash
   chmod +x arg43.sh
   ./arg43.sh Alex
   ```

**Förklaring:**  
- Här används `$1` för att hämta användarens namn från kommandoraden.

**Utmaning:** Ändra hälsningen så att den inkluderar en extra rad, t.ex. "Hoppas du får en fantastisk dag, $1!".

---

### Steg 44: Kombinera flera argument i en mening
**Mål:** Skapa ett skript som tar tre argument (t.ex. namn, stad och ålder) och kombinerar dem i en mening.

1. Skapa filen `arg44.sh`:
   ```bash
   nano arg44.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "$1 bor i $2 och är $3 år gammal."
   ```
3. Spara, avsluta, gör körbart och testa med:
   ```bash
   chmod +x arg44.sh
   ./arg44.sh Maria Stockholm 12
   ```

**Förklaring:**  
- Skriptet tar tre argument och sätter ihop dem till en mening med hjälp av variablerna `$1`, `$2` och `$3`.

**Utmaning:** Lägg till en extra rad som säger: "Vad roligt att du är $3 år!".

---

### Steg 45: Lista dina favoritdjur
**Mål:** Skapa ett skript som tar tre argument (dina favoritdjur) och skriver ut dem som en lista.

1. Skapa filen `arg45.sh`:
   ```bash
   nano arg45.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Mina favoritdjur är:"
   echo "1. $1"
   echo "2. $2"
   echo "3. $3"
   ```
3. Spara, avsluta, gör körbart och testa med:
   ```bash
   chmod +x arg45.sh
   ./arg45.sh Hund Katt Fågel
   ```

**Förklaring:**  
- Varje argument representerar ett djur som listas på en egen rad.

**Utmaning:** Ändra listan så att den använder punkter istället för siffror, t.ex. "• $1", "• $2", "• $3".

---

### Steg 46: Visa alla argument med "$@"
**Mål:** Skapa ett skript som skriver ut alla argument som skickas in.

1. Skapa filen `arg46.sh`:
   ```bash
   nano arg46.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Du angav följande argument: $@"
   ```
3. Spara, avsluta, gör körbart och testa med:
   ```bash
   chmod +x arg46.sh
   ./arg46.sh Sol Regn Vind
   ```

**Förklaring:**  
- `$@` innehåller alla argument som anges när skriptet körs.

**Utmaning:** Prova att ersätta `$@` med `$*` och observera om du får samma utskrift.

---

### Steg 47: Skapa en meny med argument
**Mål:** Skapa ett skript som tar emot tre argument och använder dem som menyval.

1. Skapa filen `arg47.sh`:
   ```bash
   nano arg47.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Huvudmeny:"
   echo "1. $1"
   echo "2. $2"
   echo "3. $3"
   ```
3. Spara, avsluta, gör körbart och testa med:
   ```bash
   chmod +x arg47.sh
   ./arg47.sh Starta Stoppa Hjälp
   ```

**Förklaring:**  
- De tre argumenten används här för att representera olika menyval.

**Utmaning:** Lägg till en extra rubrikrad innan menyn, t.ex. "Välj ett alternativ:".

---

### Steg 48: Skapa en dikt med argument
**Mål:** Använd två argument (t.ex. ett adjektiv och ett substantiv) för att skapa en enkel dikt.

1. Skapa filen `arg48.sh`:
   ```bash
   nano arg48.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Den $1 $2 dansar i vinden."
   ```
3. Spara, avsluta, gör körbart och testa med:
   ```bash
   chmod +x arg48.sh
   ./arg48.sh vackra blomma
   ```

**Förklaring:**  
- `$1` och `$2` ersätts med de ord du anger, vilket ger en dynamisk dikt.

**Utmaning:** Modifiera dikten så att den istället blir "Den $1 $2 sjunger i natten.".

---

### Steg 49: Personligt meddelande med två argument
**Mål:** Skapa ett skript som tar emot två argument: ett namn och ett meddelande, och skriver ut dem tillsammans.

1. Skapa filen `arg49.sh`:
   ```bash
   nano arg49.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Hej, $1!"
   echo "Ditt meddelande: $2"
   ```
3. Spara, avsluta, gör körbart och testa med:
   ```bash
   chmod +x arg49.sh
   ./arg49.sh Emma "Bash är superkul!"
   ```

**Förklaring:**  
- Skriptet använder `$1` för namnet och `$2` för meddelandet, vilket skapar en personlig hälsning.

**Utmaning:** Lägg till en extra rad efter meddelandet, t.ex. "Tack för att du delade med dig, $1!".

---

### Steg 50: Kombinera flera personliga argument
**Mål:** Skapa ett skript som tar emot fyra argument (t.ex. namn, favoritfärg, favoritmat och dröm) och skriver ut en personlig presentation.

1. Skapa filen `arg50.sh`:
   ```bash
   nano arg50.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Hej, $1!"
   echo "Din favoritfärg är $2, du älskar att äta $3, och du drömmer om att bli $4."
   ```
3. Spara, avsluta, gör körbart och testa med:
   ```bash
   chmod +x arg50.sh
   ./arg50.sh Johan Blå Pizza Astronaut
   ```

**Förklaring:**  
- Detta skript kombinerar fyra argument för att skapa en detaljerad personlig presentation.

**Utmaning:** Lägg till en sista rad som säger: "Fortsätt att drömma stort, $1!" och testa med egna värden.

---
