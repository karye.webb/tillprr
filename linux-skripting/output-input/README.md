# Bash-skript

## Skriva ut text med `echo`

I Linux är det vanligt att använda kommandot `echo` för att skriva ut text till terminalfönstret. I ett bash-skript kan vi använda `echo` för att skriva ut text till terminalen. Vi kan också använda `echo` för att skriva ut värden, variabler och resultat av kommandon.

Exempel:

```bash
echo "Hello, World!"
```

Resultat:

```
Hello, World!
```

## Läsa in text med `read`

I Linux är det vanligt att använda kommandot `read` för att läsa in text från användaren. I ett bash-skript kan vi använda `read` för att läsa in text från användaren. Vi kan också använda `read` för att läsa in värden i variabler.

Exempel:

```bash
read -p "Enter your name: " name
echo "Hello, $name!"
```

Resultat:

```
Enter your name: John
Hello, John!
```

## Läsa in text med argument

I Linux är det vanligt att använda argument för att skicka in värden till ett skript. I ett bash-skript kan vi använda argument för att skicka in värden till skriptet. Vi kan också använda argument för att skicka in filnamn, sökvägar och andra värden.

Exempel:

```bash
#!/bin/bash

echo "The first argument is: $1"
echo "The second argument is: $2"
```

Resultat:

```
$ ./script.sh Hello World
The first argument is: Hello
The second argument is: World
```

