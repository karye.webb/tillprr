# Använda loopar för att skapa flera filer automatiskt

Målet är att du ska lära dig att använda loopar för att skapa flera filer automatiskt. Du kommer att skapa ett skript som skapar flera filer i en mapp, och du kommer att använda olika tekniker för att göra skriptet mer flexibelt och användarvänligt.

> **Tips innan du börjar:**  
> - Öppna terminalen (t.ex. med WSL Ubuntu) och gå till din hemkatalog med:  
>   ```bash
>   cd ~
>   ```  
> - Använd en textredigerare (t.ex. `nano`) för att skapa och redigera dina skript.  
> - Kom ihåg att spara filen, göra den körbar med `chmod +x filnamn.sh` och sedan köra den med `./filnamn.sh`.

---

## Övning 5.1: Skapa ett enkelt skript med en for‑loop  
**Mål:** Skapa 10 filer i en mapp med en for‑loop.  

**Steg för steg:**
1. Öppna terminalen och gå till hemkatalogen:
   ```bash
   cd ~
   ```
2. Skapa en fil med namnet `skapa_många_filer.sh`:
   ```bash
   nano skapa_många_filer.sh
   ```
3. Skriv in följande kod:
   ```bash
   #!/bin/bash
   mkdir -p MinaFiler
   echo "Skapar 10 filer..."

   for i in {1..10}
   do
       touch MinaFiler/fil$i.txt
       echo "Fil fil$i.txt skapades!"
   done

   echo "Alla filer är klara!"
   ```
4. Spara, gör körbar med `chmod +x skapa_många_filer.sh` och kör med `./skapa_många_filer.sh`.

**Förklaring:**  
- `mkdir -p MinaFiler` skapar mappen om den inte finns.  
- `for i in {1..10}` loopar från 1 till 10, och `touch` skapar en fil med numret i namnet.

**Utmaning:** Byt ut `{1..10}` mot `{1..20}` för att skapa 20 filer istället.

---

## Övning 5.2: Fyll varje fil med en rad text  
**Mål:** Istället för att skapa tomma filer, skriv en rad text i varje fil.

**Steg för steg:**
1. Öppna samma skript eller skapa ett nytt, t.ex. `fyll_filer.sh`:
   ```bash
   nano fyll_filer.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   mkdir -p MinaFiler
   echo "Skapar 10 filer med text..."

   for i in {1..10}
   do
       echo "Detta är fil nummer $i" > MinaFiler/fil$i.txt
       echo "Fil fil$i.txt skapades med text!"
   done

   echo "Alla filer med text är klara!"
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `>` skriver över (skapar) filen med den angivna texten.

**Utmaning:** Ändra texten till att säga något annat, t.ex. "Hej, detta är fil nummer $i".

---

## Övning 5.3: Använd variabler för mapp- och filnamn  
**Mål:** Göra skriptet mer flexibelt genom att använda variabler.

**Steg för steg:**
1. Skapa ett nytt skript, t.ex. `variabel_filer.sh`:
   ```bash
   nano variabel_filer.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   mapp="MinaFiler"
   prefix="fil"

   mkdir -p $mapp
   echo "Skapar 10 filer med prefixet $prefix i mappen $mapp..."

   for i in {1..10}
   do
       echo "Detta är fil nummer $i" > $mapp/${prefix}$i.txt
       echo "Fil ${prefix}$i.txt skapades!"
   done

   echo "Alla filer har skapats i $mapp."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Variablerna `mapp` och `prefix` gör det enkelt att ändra namn på ett ställe.

**Utmaning:** Ändra värdet på `prefix` till något annat, t.ex. "dokument".

---

## Övning 5.4: Låt användaren välja mappnamnet  
**Mål:** Använd `read` för att låta användaren skriva in mappnamnet.

**Steg för steg:**
1. Skapa ett skript, t.ex. `anvandarmapp.sh`:
   ```bash
   nano anvandarmapp.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Ange namnet på mappen som ska skapas:"
   read mappnamn

   mkdir -p $mappnamn
   echo "Mappen $mappnamn har skapats!"

   for i in {1..10}
   do
       echo "Detta är fil nummer $i" > $mappnamn/fil$i.txt
       echo "Fil fil$i.txt skapades!"
   done

   echo "Alla filer har skapats i mappen $mappnamn."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `read mappnamn` låter användaren skriva in ett mappnamn, vilket sparas i variabeln.

**Utmaning:** Skapa 5 filer istället för 10 och ändra meddelandena därefter.

---

## Övning 5.5: Använd `seq` istället för måttalet i loop  
**Mål:** Lär dig en alternativ loop-syntax med `seq`.

**Steg för steg:**
1. Skapa ett skript, t.ex. `seq_filer.sh`:
   ```bash
   nano seq_filer.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   mkdir -p MinaFiler
   echo "Skapar 10 filer med seq..."

   for i in $(seq 1 10)
   do
       echo "Detta är fil nummer $i" > MinaFiler/fil$i.txt
       echo "Fil fil$i.txt skapades!"
   done

   echo "Alla filer skapade med seq!"
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `$(seq 1 10)` genererar en sekvens från 1 till 10.

**Utmaning:** Prova att ändra `seq 1 10` till `seq 1 20`.

---

## Övning 5.6: Skapa filer med olika filändelser  
**Mål:** Skapa filer med olika ändelser genom att använda en loop.

**Steg för steg:**
1. Skapa ett skript, t.ex. `olika_andedelser.sh`:
   ```bash
   nano olika_andedelser.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   mkdir -p MinaFiler
   echo "Skapar 10 filer med .txt och .log..."

   for i in {1..10}
   do
       echo "Detta är textfil nummer $i" > MinaFiler/fil$i.txt
       echo "Detta är logfil nummer $i" > MinaFiler/fil$i.log
       echo "Filer fil$i.txt och fil$i.log skapades!"
   done

   echo "Alla filer är klara!"
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Två kommandon inuti loopen skapar filer med olika ändelser.

**Utmaning:** Skapa även en .data-fil för varje iteration.

---

## Övning 5.7: Skapa en undermapp och filer inuti den  
**Mål:** Skapa en undermapp och använd loopen för att skapa filer där.

**Steg för steg:**
1. Skapa ett skript, t.ex. `undermapp_filer.sh`:
   ```bash
   nano undermapp_filer.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   mkdir -p MinaFiler/UnderMapp
   echo "Skapar 10 filer i undermappen UnderMapp..."

   for i in {1..10}
   do
       echo "Detta är fil nummer $i" > MinaFiler/UnderMapp/fil$i.txt
       echo "Fil fil$i.txt skapades i UnderMapp!"
   done

   echo "Alla filer i undermappen har skapats!"
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Sökvägen `MinaFiler/UnderMapp/fil$i.txt` skapar filerna i en undermapp.

**Utmaning:** Lägg till ett meddelande som skriver ut innehållet i undermappen med `ls MinaFiler/UnderMapp`.

---

## Övning 5.8: Ändra loopens steglängd  
**Mål:** Skapa filer med jämna nummer (t.ex. 2, 4, 6, …) genom att ändra steglängden.

**Steg för steg:**
1. Skapa ett skript, t.ex. `steglangd.sh`:
   ```bash
   nano steglangd.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   mkdir -p MinaFiler
   echo "Skapar filer med jämna nummer från 2 till 20..."

   for i in $(seq 2 2 20)
   do
       echo "Detta är fil nummer $i" > MinaFiler/fil$i.txt
       echo "Fil fil$i.txt skapades!"
   done

   echo "Alla jämna nummer-filer har skapats!"
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `seq 2 2 20` genererar en sekvens som startar på 2, ökar med 2 och slutar vid 20.

**Utmaning:** Försök att ändra steget från 2 till 3 (dvs. 3, 6, 9, …).

---

## Övning 5.9: Skapa en fil som listar alla skapade filnamn  
**Mål:** Efter att loopen skapat filerna, skriv ut en sammanfattning i en separat fil.

**Steg för steg:**
1. Skapa ett skript, t.ex. `lista_filer.sh`:
   ```bash
   nano lista_filer.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   mkdir -p MinaFiler
   echo "Skapar 10 filer och listar deras namn..."

   for i in {1..10}
   do
       echo "Detta är fil nummer $i" > MinaFiler/fil$i.txt
       echo "fil$i.txt" >> MinaFiler/sammanfattning.txt
   done

   echo "Följande filer skapades:"
   cat MinaFiler/sammanfattning.txt
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Varje iteration lägger till filnamnet i filen `sammanfattning.txt` med `>>`.

**Utmaning:** Lägg till en rad i början av `sammanfattning.txt` som säger "Skapade filer:".

---

## Övning 5.10: Skapa en loop med hjälp av en räknare variabel  
**Mål:** Visa hur du kan använda en räknare som ökar med 1 i en for‑loop.

**Steg för steg:**
1. Skapa ett skript, t.ex. `raknare.sh`:
   ```bash
   nano raknare.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   mkdir -p MinaFiler
   echo "Skapar 10 filer med en räknare..."

   counter=1
   for i in {1..10}
   do
       echo "Fil nummer $counter" > MinaFiler/fil$counter.txt
       echo "Fil fil$counter.txt skapades!"
       counter=$((counter+1))
   done

   echo "Alla filer har skapats med hjälp av en räknare!"
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Variabeln `counter` används för att hålla koll på antalet filer och ökas med 1 varje gång.

**Utmaning:** Försök att ändra initialvärdet eller ökningssteget.

---

## Övning 5.11: Låt användaren välja hur många filer som ska skapas  
**Mål:** Gör skriptet interaktivt så att användaren bestämmer antalet filer.

**Steg för steg:**
1. Skapa ett skript, t.ex. `antal_filer.sh`:
   ```bash
   nano antal_filer.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Hur många filer vill du skapa?"
   read antal

   mkdir -p MinaFiler
   echo "Skapar $antal filer..."

   for i in $(seq 1 $antal)
   do
       echo "Detta är fil nummer $i" > MinaFiler/fil$i.txt
       echo "Fil fil$i.txt skapades!"
   done

   echo "Alla $antal filer har skapats!"
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `read antal` låter användaren ange ett tal, och `seq 1 $antal` skapar en sekvens från 1 till det talet.

**Utmaning:** Testa med olika tal!

---

## Övning 5.12: Skapa filer med förbestämt innehåll med en variabel  
**Mål:** Använd en variabel för att lagra innehållet som skrivs in i varje fil.

**Steg för steg:**
1. Skapa ett skript, t.ex. `innehall_variabel.sh`:
   ```bash
   nano innehall_variabel.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   innehall="Hej! Detta är en automatisk skapad fil."
   mkdir -p MinaFiler
   echo "Skapar 10 filer med innehåll från en variabel..."

   for i in {1..10}
   do
       echo "$innehall Fil nummer $i" > MinaFiler/fil$i.txt
       echo "Fil fil$i.txt skapades med innehåll!"
   done

   echo "Alla filer har skapats med förbestämt innehåll!"
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Variabeln `innehall` håller texten som skrivs in i varje fil.

**Utmaning:** Ändra texten i variabeln och se att alla filer uppdateras med det nya innehållet.

---

## Övning 5.13: Skapa filer med ledande nollor i numreringen  
**Mål:** Skapa filer med namn som t.ex. fil01.txt, fil02.txt, etc.

**Steg för steg:**
1. Skapa ett skript, t.ex. `ledande_nollor.sh`:
   ```bash
   nano ledande_nollor.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   mkdir -p MinaFiler
   echo "Skapar 10 filer med ledande nollor..."

   for i in $(seq -w 1 10)
   do
       echo "Detta är fil nummer $i" > MinaFiler/fil$i.txt
       echo "Fil fil$i.txt skapades!"
   done

   echo "Filer med ledande nollor har skapats!"
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `seq -w 1 10` säkerställer att siffrorna har fast bredd (01, 02, …).

**Utmaning:** Ändra antalet filer och se att formatet bibehålls.

---

## Övning 5.14: Skapa en fil som summerar alla filnamn  
**Mål:** Efter att ha skapat filerna, skapa en extra fil som listar alla filnamn.

**Steg för steg:**
1. Skapa ett skript, t.ex. `summering.sh`:
   ```bash
   nano summering.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   mkdir -p MinaFiler
   echo "Skapar 10 filer och en summeringsfil..."

   > MinaFiler/summering.txt  # Töm summeringsfilen

   for i in {1..10}
   do
       echo "Detta är fil nummer $i" > MinaFiler/fil$i.txt
       echo "fil$i.txt" >> MinaFiler/summering.txt
   done

   echo "Summeringsfilen visar följande filer:"
   cat MinaFiler/summering.txt
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `>>` används för att lägga till filnamn i summeringsfilen utan att skriva över tidigare innehåll.

**Utmaning:** Lägg till en rubrik i summeringsfilen innan listan med filnamn.

---

## Övning 5.15: Lägg till en tom rad mellan varje filinnehåll  
**Mål:** Använd `echo ""` för att lägga till en tom rad i varje fil.

**Steg för steg:**
1. Skapa ett skript, t.ex. `tom_rader.sh`:
   ```bash
   nano tom_rader.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   mkdir -p MinaFiler
   echo "Skapar 10 filer med en tom rad efter texten..."

   for i in {1..10}
   do
       echo "Detta är fil nummer $i" > MinaFiler/fil$i.txt
       echo "" >> MinaFiler/fil$i.txt
       echo "En tom rad lades till i fil$i.txt"
   done

   echo "Alla filer har skapats med en extra tom rad!"
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Ett tomt `echo ""` skapar en tom rad i filen.

**Utmaning:** Lägg till två tomma rader istället för en.

---

## Övning 5.16: Låt användaren välja antalet filer  
**Mål:** Kombinera användarinput med en for‑loop för att skapa ett dynamiskt antal filer.

**Steg för steg:**
1. Skapa ett skript, t.ex. `valfritt_antal.sh`:
   ```bash
   nano valfritt_antal.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Hur många filer vill du skapa?"
   read antal

   mkdir -p MinaFiler
   echo "Skapar $antal filer..."

   for i in $(seq 1 $antal)
   do
       echo "Detta är fil nummer $i" > MinaFiler/fil$i.txt
       echo "Fil fil$i.txt skapades!"
   done

   echo "Alla $antal filer har skapats!"
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `read antal` tar in ett tal från användaren, och `seq` används för att loopa från 1 till det talet.

**Utmaning:** Testa med olika värden och kontrollera resultaten.

---

## Övning 5.17: Skapa en loop med ett fast intervall (exempelvis varannan fil)  
**Mål:** Använd `seq` för att skapa filer med ett steg, t.ex. varannan.

**Steg för steg:**
1. Skapa ett skript, t.ex. `varannan.sh`:
   ```bash
   nano varannan.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   mkdir -p MinaFiler
   echo "Skapar filer varannan från 2 till 20..."

   for i in $(seq 2 2 20)
   do
       echo "Detta är fil nummer $i" > MinaFiler/fil$i.txt
       echo "Fil fil$i.txt skapades!"
   done

   echo "Filer med jämna nummer har skapats!"
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `seq 2 2 20` genererar siffror 2, 4, 6, …, 20.

**Utmaning:** Ändra steglängden från 2 till 3 (t.ex. 3, 6, 9, …).

---

## Övning 5.18: Skapa filer med ett unikt innehåll baserat på loopindex  
**Mål:** Variera innehållet i varje fil med hjälp av loopvariabeln.

**Steg för steg:**
1. Skapa ett skript, t.ex. `unik_innehall.sh`:
   ```bash
   nano unik_innehall.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   mkdir -p MinaFiler
   echo "Skapar 10 filer med unikt innehåll..."

   for i in {1..10}
   do
       echo "Detta är den unika texten för fil nummer $i" > MinaFiler/fil$i.txt
       echo "Fil fil$i.txt skapades med unikt innehåll!"
   done

   echo "Alla filer har skapats med unikt innehåll!"
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Varje fil får en unik rad baserat på vilket nummer loopen är på.

**Utmaning:** Lägg till en extra rad i varje fil med samma innehåll.

---

## Övning 5.19: Skapa en "meny" med skapade filnamn  
**Mål:** Efter att filerna skapats, skriv ut en lista med alla filnamn på skärmen.

**Steg för steg:**
1. Skapa ett skript, t.ex. `lista_filer_loop.sh`:
   ```bash
   nano lista_filer_loop.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   mkdir -p MinaFiler
   echo "Skapar 10 filer..."

   for i in {1..10}
   do
       echo "Detta är fil nummer $i" > MinaFiler/fil$i.txt
   done

   echo "Följande filer skapades:"
   for i in {1..10}
   do
       echo "MinaFiler/fil$i.txt"
   done
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- En andra loop används för att skriva ut sökvägarna till de skapade filerna.

**Utmaning:** Använd `cat` för att visa innehållet i varje fil istället.

---

## Övning 5.20: Avslutande repetition – Kombinera allt i ett komplett skript  
**Mål:** Skapa ett skript som låter användaren välja mappnamn och antal filer, skapar filerna med unikt innehåll och skriver ut en sammanfattning.

**Steg för steg:**
1. Skapa ett skript, t.ex. `fullt_loop.sh`:
   ```bash
   nano fullt_loop.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Ange mappnamn där filerna ska skapas:"
   read mappnamn
   echo "Ange hur många filer du vill skapa:"
   read antal

   mkdir -p $mappnamn
   echo "Skapar $antal filer i mappen $mappnamn..."

   for i in $(seq 1 $antal)
   do
       echo "Detta är den unika texten för fil nummer $i" > $mappnamn/fil$i.txt
       echo "Fil fil$i.txt skapades!"
   done

   echo "Alla $antal filer har skapats i mappen $mappnamn. Här är en sammanfattning:"
   ls $mappnamn
   ```
3. Spara, gör körbar med `chmod +x fullt_loop.sh` och kör skriptet med `./fullt_loop.sh`.

**Förklaring:**  
- Detta skript kombinerar alla tidigare moment: användarinput, variabler, loopar och utskrift av resultat.

**Utmaning:** Försök att redigera skriptet så att varje fil får två rader text (använd `>>` för att lägga till den andra raden).
