# Enkla villkor i bash

Målet med dessa övningar är att lära sig använda enkla villkor i bash-skript. Du kommer att använda `if`-satser för att jämföra numeriska värden och ge olika svar beroende på resultatet. Genom att använda `if`, `elif` och `else` kan du skapa mer avancerade skript som reagerar på olika situationer.

> **Allmänna tips innan du börjar:**  
> - Öppna terminalen (t.ex. WSL Ubuntu) och gå till din hemkatalog med:  
>   ```bash
>   cd ~
>   ```  
> - Använd en textredigerare, t.ex. `nano`, för att skapa och redigera dina skript.  
> - Spara filen, gör den körbar med:
>   ```bash
>   chmod +x filnamn.sh
>   ```  
> - Kör skriptet med:
>   ```bash
>   ./filnamn.sh
>   ```

---

## Övning 8.1: Enkel ålderskontroll med if-else  
**Mål:** Lär dig att jämföra ett tal med 18 och ge ett svar.  

**Steg-för-steg:**
1. Skapa ett skript med namnet `alder1.sh`:
   ```bash
   nano alder1.sh
   ```
2. Skriv in följande kod:
   ```bash
   #!/bin/bash
   echo "Hur gammal är du?"
   read alder

   if [ "$alder" -ge 18 ]; then
       echo "Du är myndig!"
   else
       echo "Du är under 18 år."
   fi
   ```
3. Spara, gör körbart och kör skriptet:
   ```bash
   chmod +x alder1.sh
   ./alder1.sh
   ```

**Förklaring:**  
- `read alder` tar emot användarens ålder.  
- `if [ "$alder" -ge 18 ]` kontrollerar om åldern är större än eller lika med 18.  
- `else` hanterar fallet om användaren är yngre.

**Utmaning:** Testa att skriva in olika tal (t.ex. 15, 18, 20) för att se olika svar.

---

## Övning 8.2: Exakt 18-åring med if-elif-else  
**Mål:** Ge ett särskilt meddelande för exakt 18 år.

**Steg-för-steg:**
1. Skapa ett skript med namnet `alder2.sh`:
   ```bash
   nano alder2.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Hur gammal är du?"
   read alder

   if [ "$alder" -gt 18 ]; then
       echo "Du är vuxen!"
   elif [ "$alder" -eq 18 ]; then
       echo "Grattis! Du är precis myndig!"
   else
       echo "Du är under 18 år."
   fi
   ```
3. Spara, gör körbart och kör skriptet.

**Förklaring:**  
- `elif [ "$alder" -eq 18 ]` kontrollerar om åldern är exakt 18.

**Utmaning:** Lägg till en kommentar som säger "Välkommen till vuxenvärlden!" om åldern är över 18.

---

## Övning 8.3: Flera åldersgrupper med if-elif-else  
**Mål:** Dela in ålder i tre grupper: barn, tonåring och vuxen.

**Steg-för-steg:**
1. Skapa ett skript med namnet `alder3.sh`:
   ```bash
   nano alder3.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Ange din ålder:"
   read alder

   if [ "$alder" -lt 13 ]; then
       echo "Du är ett barn."
   elif [ "$alder" -lt 18 ]; then
       echo "Du är en tonåring."
   else
       echo "Du är vuxen."
   fi
   ```
3. Spara, gör körbart och kör skriptet.

**Förklaring:**  
- `-lt` betyder "mindre än".  
- Flera elif-satser delar in åldern i olika grupper.

**Utmaning:** Ändra gränserna, t.ex. barn < 10, ungdom 10–17, vuxen ≥ 18.

---

## Övning 8.4: Kontrollera om ålder är jämn eller udda  
**Mål:** Använd modulusoperatorn för att avgöra om åldern är jämn eller udda.

**Steg-för-steg:**
1. Skapa ett skript med namnet `alder_jamn_eller_udda.sh`:
   ```bash
   nano alder_jamn_eller_udda.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Hur gammal är du?"
   read alder

   if [ $((alder % 2)) -eq 0 ]; then
       echo "Din ålder är jämn."
   else
       echo "Din ålder är udda."
   fi
   ```
3. Spara, gör körbart och kör skriptet.

**Förklaring:**  
- `$((alder % 2))` beräknar resten när `alder` delas med 2.  
- Om resten är 0 är talet jämnt.

**Utmaning:** Testa med olika åldrar för att se rätt meddelande.

---

## Övning 8.5: Kontrollera negativ ålder  
**Mål:** Lägg till en kontroll för att säkerställa att åldern är ett positivt tal.

**Steg-för-steg:**
1. Skapa ett skript med namnet `alder_kontroll.sh`:
   ```bash
   nano alder_kontroll.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Ange din ålder:"
   read alder

   if [ "$alder" -lt 0 ]; then
       echo "Fel: Åldern kan inte vara negativ!"
   elif [ "$alder" -ge 18 ]; then
       echo "Du är myndig."
   else
       echo "Du är under 18 år."
   fi
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Först kontrolleras att åldern inte är negativ med `-lt 0`.

**Utmaning:** Lägg till ett meddelande om åldern är exakt 0 (t.ex. "Du är nyfödd!").

---

## Övning 8.6: Använd dubbla hakparenteser för jämförelse  
**Mål:** Visa hur man använder `[[ ... ]]` istället för `[ ... ]` för jämförelser.

**Steg-för-steg:**
1. Skapa ett skript med namnet `alder_dubbla.sh`:
   ```bash
   nano alder_dubbla.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Ange din ålder:"
   read alder

   if [[ $alder -ge 18 ]]; then
       echo "Du är myndig!"
   else
       echo "Du är under 18 år."
   fi
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Dubbelhakparenteser `[[ ... ]]` erbjuder fler funktioner och är ofta säkrare i bash.

**Utmaning:** Jämför om åldern är exakt 18 med `[[ $alder -eq 18 ]]`.

---

## Övning 8.7: Ange ålder och ge flera svar beroende på olika intervall  
**Mål:** Dela in åldern i fyra grupper och ge specifika meddelanden.

**Steg-för-steg:**
1. Skapa ett skript med namnet `alder_intervall.sh`:
   ```bash
   nano alder_intervall.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Hur gammal är du?"
   read alder

   if [[ $alder -lt 10 ]]; then
       echo "Du är väldigt ung!"
   elif [[ $alder -lt 15 ]]; then
       echo "Du är ett barn."
   elif [[ $alder -lt 18 ]]; then
       echo "Du är nästan vuxen."
   else
       echo "Du är vuxen."
   fi
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Flera elif-satser delar in åldern i olika intervall med tydliga meddelanden.

**Utmaning:** Justera åldersgränserna och meddelandena för att passa egna idéer.

---

## Övning 8.8: Fråga om både ålder och favoritmat  
**Mål:** Använd två `read`-kommandon och ge svar baserat på ålder och favoritmat.

**Steg-för-steg:**
1. Skapa ett skript med namnet `alder_och_mat.sh`:
   ```bash
   nano alder_och_mat.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Hur gammal är du?"
   read alder
   echo "Vad är din favoritmat?"
   read favoritmat

   if [[ $alder -ge 18 ]]; then
       echo "Du är vuxen och gillar $favoritmat. Låter gott!"
   else
       echo "Du är under 18 och gillar $favoritmat. Smakfullt val!"
   fi
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Detta skript kombinerar två inmatningar och ger ett svar som beror på båda.

**Utmaning:** Lägg till ett extra svar om åldern är exakt 18.

---

## Övning 8.9: Kontrollera om åldern är mellan två värden  
**Mål:** Använd en kombination av villkor för att kolla om åldern ligger inom ett visst intervall.

**Steg-för-steg:**
1. Skapa ett skript med namnet `intervall_koll.sh`:
   ```bash
   nano intervall_koll.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Ange din ålder:"
   read alder

   if [[ $alder -ge 13 && $alder -le 19 ]]; then
       echo "Du är en tonåring."
   else
       echo "Du är inte en tonåring."
   fi
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `&&` används för att kombinera två villkor så att båda måste vara sanna.

**Utmaning:** Prova med olika åldrar för att se om det stämmer.

---

## Övning 8.10: Använd en nested (inbäddad) if-sats  
**Mål:** Visa hur man kan använda en if-sats inuti en annan if-sats.

**Steg-för-steg:**
1. Skapa ett skript med namnet `nested_if.sh`:
   ```bash
   nano nested_if.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Hur gammal är du?"
   read alder

   if [[ $alder -ge 18 ]]; then
       echo "Du är vuxen!"
       if [[ $alder -gt 65 ]]; then
           echo "Och du är också senior!"
       fi
   else
       echo "Du är under 18."
   fi
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Den inre if-satsen kontrollerar ett ytterligare villkor när huvudvillkoret är sant.

**Utmaning:** Lägg till en extra kontroll för åldrar mellan 18 och 65.

---

## Övning 8.11: Jämför åldersinmatning med en fördefinierad variabel  
**Mål:** Använd en variabel för en "minimiålder" och jämför den med användarens inmatning.

**Steg-för-steg:**
1. Skapa ett skript med namnet `minimi_alder.sh`:
   ```bash
   nano minimi_alder.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   minimi=18
   echo "Hur gammal är du?"
   read alder

   if [[ $alder -ge $minimi ]]; then
       echo "Du är myndig (minimi är $minimi)."
   else
       echo "Du är under myndighetsåldern (minimi är $minimi)."
   fi
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Variabeln `minimi` gör det enkelt att ändra åldersgränsen på ett ställe.

**Utmaning:** Byt ut 18 mot ett annat tal och testa igen.

---

## Övning 8.12: Ange ålder med ett standardvärde  
**Mål:** Ge användaren möjlighet att lämna inmatning tom med ett standardvärde.

**Steg-för-steg:**
1. Skapa ett skript, t.ex. `standard_alder.sh`:
   ```bash
   nano standard_alder.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Hur gammal är du? (tryck Enter om du inte vill svara)"
   read alder
   if [[ -z "$alder" ]]; then
       alder=18
       echo "Inget svar, vi antar att du är 18."
   fi

   if [[ $alder -ge 18 ]]; then
       echo "Du är myndig!"
   else
       echo "Du är under 18 år."
   fi
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `-z "$alder"` kontrollerar om variabeln är tom.

**Utmaning:** Ändra standardvärdet till 21 och testa.

---

## Övning 8.13: Skriv ut olika meddelanden beroende på ålder med flera elif  
**Mål:** Ge mer nyanserade svar beroende på ålder.

**Steg-för-steg:**
1. Skapa ett skript, t.ex. `flera_svar.sh`:
   ```bash
   nano flera_svar.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Ange din ålder:"
   read alder

   if [[ $alder -lt 10 ]]; then
       echo "Du är ett litet barn."
   elif [[ $alder -lt 18 ]]; then
       echo "Du är en tonåring."
   elif [[ $alder -eq 18 ]]; then
       echo "Grattis, du är precis myndig!"
   else
       echo "Du är vuxen."
   fi
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Flera elif-satser gör att vi kan ge olika svar för olika åldersintervall.

**Utmaning:** Justera gränserna och meddelandena för att skapa egna kategorier.

---

## Övning 8.14: Använd en if-sats för att kontrollera om åldern är exakt ett visst värde  
**Mål:** Ge ett specifikt meddelande om användaren anger ett visst tal.

**Steg-för-steg:**
1. Skapa ett skript, t.ex. `exakt_alder.sh`:
   ```bash
   nano exakt_alder.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Hur gammal är du?"
   read alder

   if [[ $alder -eq 16 ]]; then
       echo "Wow, du är 16 – en spännande ålder!"
   else
       echo "Du är inte 16."
   fi
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `-eq` jämför om åldern är exakt lika med ett specifikt tal.

**Utmaning:** Ändra talet (t.ex. 12) och anpassa meddelandet.

---

## Övning 8.15: Använd en if-sats för att kontrollera om åldern är större än ett värde  
**Mål:** Ge ett meddelande om användaren är äldre än 30 år.

**Steg-för-steg:**
1. Skapa ett skript, t.ex. `over_30.sh`:
   ```bash
   nano over_30.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Hur gammal är du?"
   read alder

   if [[ $alder -gt 30 ]]; then
       echo "Du är över 30 år."
   else
       echo "Du är 30 år eller yngre."
   fi
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `-gt` betyder "större än".

**Utmaning:** Ändra villkoret till 40 istället och se skillnaden.

---

## Övning 8.16: Använd if-sats med logisk OCH (&&)  
**Mål:** Kontrollera om åldern ligger mellan 20 och 30 år.

**Steg-för-steg:**
1. Skapa ett skript, t.ex. `mellan_20_30.sh`:
   ```bash
   nano mellan_20_30.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Hur gammal är du?"
   read alder

   if [[ $alder -ge 20 && $alder -le 30 ]]; then
       echo "Din ålder är mellan 20 och 30."
   else
       echo "Din ålder är utanför intervallet 20-30."
   fi
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `&&` används för att kontrollera att båda villkoren är sanna.

**Utmaning:** Ändra intervallet, t.ex. 25 till 35.

---

## Övning 8.17: Kontrollera ålder med negativt svar med if-sats  
**Mål:** Ge ett felmeddelande om användaren matar in en ogiltig (negativ) ålder.

**Steg-för-steg:**
1. Skapa ett skript, t.ex. `ogiltig_alder.sh`:
   ```bash
   nano ogiltig_alder.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Ange din ålder:"
   read alder

   if [[ $alder -lt 0 ]]; then
       echo "Fel: Åldern kan inte vara negativ!"
   elif [[ $alder -ge 18 ]]; then
       echo "Du är myndig."
   else
       echo "Du är under 18 år."
   fi
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Det första if-villkoret hanterar ogiltig (negativ) inmatning.

**Utmaning:** Lägg till en kontroll för att se om inmatningen är tom.

---

## Övning 8.18: Använd en variabel för att lagra åldersgränsen  
**Mål:** Gör det enkelt att ändra åldersgränsen genom att använda en variabel.

**Steg-för-steg:**
1. Skapa ett skript, t.ex. `grans_varde.sh`:
   ```bash
   nano grans_varde.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   grens=18
   echo "Hur gammal är du?"
   read alder

   if [[ $alder -ge $grens ]]; then
       echo "Du är myndig (gränsen är $grens år)."
   else
       echo "Du är under $grens år."
   fi
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Variabeln `grens` gör det enkelt att ändra den kritiska åldern på ett ställe.

**Utmaning:** Ändra gränsen till 21 och uppdatera meddelandet.

---

## Övning 8.19: Kombinera ålderskontroll med extra inmatning  
**Mål:** Fråga om ålder och en extra fråga (t.ex. favoritfärg) och ge ett sammansatt svar.

**Steg-för-steg:**
1. Skapa ett skript, t.ex. `alder_och_farg.sh`:
   ```bash
   nano alder_och_farg.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Hur gammal är du?"
   read alder
   echo "Vilken är din favoritfärg?"
   read farg

   if [[ $alder -ge 18 ]]; then
       echo "Du är myndig och din favoritfärg är $farg. Coolt!"
   else
       echo "Du är under 18 och din favoritfärg är $farg. Färgglatt!"
   fi
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Två `read`-kommandon samlar in olika uppgifter, och if-satsen ger ett sammansatt svar.

**Utmaning:** Lägg till en extra fråga (t.ex. favoritdjur) och inkludera det i meddelandet.

---

## Övning 8.20: Avslutande interaktivt skript – Personlig respons  
**Mål:** Skapa ett komplett skript där användaren får ange sin ålder, och skriptet ger en personlig respons med flera if/elif-satser.

**Steg-för-steg:**
1. Skapa ett skript, t.ex. `personlig_respons.sh`:
   ```bash
   nano personlig_respons.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Hur gammal är du?"
   read alder

   if [[ $alder -lt 10 ]]; then
       echo "Hej, du är väldigt ung!"
   elif [[ $alder -lt 18 ]]; then
       echo "Hej, du är en tonåring. Njut av dina år!"
   elif [[ $alder -eq 18 ]]; then
       echo "Grattis, du är precis myndig! Välkommen till vuxenvärlden!"
   else
       echo "Hej, du är vuxen. Hoppas du har många spännande äventyr framför dig!"
   fi
   ```
3. Spara, gör körbar och kör skriptet:
   ```bash
   chmod +x personlig_respons.sh
   ./personlig_respons.sh
   ```

**Förklaring:**  
- Detta skript kombinerar flera if/elif-satser för att ge ett personligt svar baserat på användarens ålder.

**Utmaning:** Utöka skriptet genom att be användaren ange sitt namn och inkludera det i svaret.
