# Skapa mappar och filer

Målet med denna övning är att skapa enkla skript som skapar mappar och filer. Du kommer att använda kommandon som `mkdir` för att skapa mappar och `touch` för att skapa filer. Du kommer också att lära dig att skriva text i filer och visa innehållet i dem.

> **Tips innan du börjar:**  
> - Öppna terminalen (t.ex. med WSL Ubuntu) och gå till din hemkatalog med:
>   ```bash
>   cd ~
>   ```
> - Använd en textredigerare (t.ex. `nano`) för att skapa och redigera dina skript.
> - Kom ihåg att spara dina filer, göra dem körbara med:
>   ```bash
>   chmod +x skriptets_namn.sh
>   ```
> - Kör skripten med:
>   ```bash
>   ./skriptets_namn.sh
>   ```

---

## Övning 1: Skapa ett enkelt skript som skapar en mapp  
**Mål:** Lära sig att skapa en mapp med kommandot `mkdir`.  

**Steg för steg:**

1. **Öppna terminalen** och gå till hemkatalogen:
   ```bash
   cd ~
   ```
2. **Skapa ett nytt skript** med namnet `skapa_mapp.sh`:
   ```bash
   nano skapa_mapp.sh
   ```
3. **Skriv in följande kod:**
   ```bash
   #!/bin/bash
   echo "Skapar mappen MinMapp..."
   mkdir MinMapp
   echo "Mappen MinMapp har skapats!"
   ```
4. **Spara** filen (CTRL+O, Enter) och **avsluta** (CTRL+X).

5. **Gör skriptet körbart** och kör det:
   ```bash
   chmod +x skapa_mapp.sh
   ./skapa_mapp.sh
   ```

**Förklaring:**  
- `mkdir MinMapp` skapar en mapp som heter *MinMapp*.  
- `echo` visar meddelanden så att du ser vad som händer.

**Utmaning:** Ändra mappnamnet från *MinMapp* till något annat, t.ex. *MinCoolaMapp*.

---

## Övning 2: Skapa en fil med `touch`  
**Mål:** Lära sig att skapa en tom fil.  

**Steg för steg:**

1. **Skapa ett nytt skript** med namnet `skapa_fil.sh`:
   ```bash
   nano skapa_fil.sh
   ```
2. **Skriv in följande kod:**
   ```bash
   #!/bin/bash
   echo "Skapar en tom fil..."
   touch fil.txt
   echo "Filen fil.txt har skapats!"
   ```
3. **Spara, gör körbar** och **kör skriptet**:
   ```bash
   chmod +x skapa_fil.sh
   ./skapa_fil.sh
   ```

**Förklaring:**  
- `touch fil.txt` skapar en tom fil med namnet *fil.txt*.

**Utmaning:** Ändra filnamnet till något personligt, t.ex. *min_anteckning.txt*.

---

## Övning 3: Skapa både mapp och filer  
**Mål:** Skapa en mapp och sedan tre filer inuti den.  

**Steg för steg:**

1. **Skapa ett nytt skript** med namnet `skapa_filer.sh`:
   ```bash
   nano skapa_filer.sh
   ```
2. **Skriv in koden:**
   ```bash
   #!/bin/bash
   echo "Skapar mappen MinMapp..."
   mkdir MinMapp

   echo "Skapar tre textfiler i MinMapp..."
   touch MinMapp/fil1.txt
   touch MinMapp/fil2.txt
   touch MinMapp/fil3.txt

   echo "Klart! Filerna har skapats i mappen MinMapp."
   ```
3. **Spara, gör körbart** och **kör skriptet**:
   ```bash
   chmod +x skapa_filer.sh
   ./skapa_filer.sh
   ```

**Förklaring:**  
- Varje `touch`-kommando skapar en tom fil i mappen *MinMapp*.

**Utmaning:** Prova att skapa fyra filer istället för tre genom att lägga till en extra `touch`-rad.

---

## Övning 4: Verifiera att filerna skapats  
**Mål:** Lära sig att navigera in i en mapp och lista dess innehåll.

**Steg för steg:**

1. **Navigera till mappen:**
   ```bash
   cd MinMapp
   ```
2. **Lista filerna:**
   ```bash
   ls
   ```
3. **Verifiera** att du ser:
   ```
   fil1.txt  fil2.txt  fil3.txt
   ```

**Förklaring:**  
- `cd` byter mapp, och `ls` listar innehållet i mappen.

**Utmaning:** Använd kommandot `pwd` för att se den fulla sökvägen till mappen.

---

## Övning 5: Lägg till meddelanden med `echo`  
**Mål:** Visa tydliga meddelanden under skapandet av mappar och filer.

**Steg för steg:**

1. **Öppna ditt skript** `skapa_filer.sh` och lägg till extra `echo`-kommandon:
   ```bash
   #!/bin/bash
   echo "Startar skapelseprocessen..."

   echo "Skapar mappen MinMapp..."
   mkdir MinMapp
   echo "Mappen MinMapp är klar!"

   echo "Skapar fil1.txt..."
   touch MinMapp/fil1.txt
   echo "fil1.txt skapad!"

   echo "Skapar fil2.txt..."
   touch MinMapp/fil2.txt
   echo "fil2.txt skapad!"

   echo "Skapar fil3.txt..."
   touch MinMapp/fil3.txt
   echo "fil3.txt skapad!"

   echo "Alla filer har skapats i MinMapp."
   ```
2. **Spara, gör körbar** och **kör skriptet**.

**Förklaring:**  
- Extra meddelanden gör det lättare att följa vad som händer steg för steg.

**Utmaning:** Lägg till en sista rad som skriver "Skriptet är färdigt!" i slutet.

---

## Övning 6: Använd variabler för mapp- och filnamn  
**Mål:** Göra skriptet mer flexibelt med variabler.

**Steg för steg:**

1. **Skapa ett nytt skript** `variabel_script.sh`:
   ```bash
   nano variabel_script.sh
   ```
2. **Skriv in följande kod:**
   ```bash
   #!/bin/bash
   mapp="MinMapp"
   fil1="fil1.txt"
   fil2="fil2.txt"
   fil3="fil3.txt"

   echo "Skapar mappen $mapp..."
   mkdir $mapp

   echo "Skapar tre filer: $fil1, $fil2, och $fil3..."
   touch $mapp/$fil1
   touch $mapp/$fil2
   touch $mapp/$fil3

   echo "Filerna har skapats i mappen $mapp."
   ```
3. **Spara, gör körbar** och **kör skriptet**.

**Förklaring:**  
- Variablerna gör det enkelt att ändra namn på mappen och filerna på ett ställe.

**Utmaning:** Ändra värdena på variablerna och se hur utskrifterna ändras.

---

## Övning 7: Låt användaren välja mappnamnet  
**Mål:** Använd `read` för att ta emot användarens inmatning.

**Steg för steg:**

1. **Skapa ett skript** `anvandarmapp.sh`:
   ```bash
   nano anvandarmapp.sh
   ```
2. **Skriv in koden:**
   ```bash
   #!/bin/bash
   echo "Ange namnet på mappen som ska skapas:"
   read mappnamn

   echo "Skapar mappen $mappnamn..."
   mkdir $mappnamn

   echo "Skapar tre filer i mappen $mappnamn..."
   touch $mappnamn/fil1.txt
   touch $mappnamn/fil2.txt
   touch $mappnamn/fil3.txt

   echo "Filerna har skapats i mappen $mappnamn."
   ```
3. **Spara, gör körbar** och **kör skriptet**.

**Förklaring:**  
- `read mappnamn` låter användaren skriva in ett mappnamn som lagras i variabeln.

**Utmaning:** Testa att skriva in olika mappnamn vid körning.

---

## Övning 8: Låt användaren välja filnamn  
**Mål:** Använd `read` för att ta emot filnamn från användaren.

**Steg för steg:**

1. **Skapa ett skript** `anvandarfiler.sh`:
   ```bash
   nano anvandarfiler.sh
   ```
2. **Skriv in koden:**
   ```bash
   #!/bin/bash
   echo "Ange mappnamn:"
   read mappnamn
   mkdir $mappnamn

   echo "Ange namnet för den första filen:"
   read fil1
   echo "Ange namnet för den andra filen:"
   read fil2
   echo "Ange namnet för den tredje filen:"
   read fil3

   touch $mappnamn/$fil1
   touch $mappnamn/$fil2
   touch $mappnamn/$fil3

   echo "Filerna $fil1, $fil2 och $fil3 har skapats i mappen $mappnamn."
   ```
3. **Spara, gör körbar** och **kör skriptet**.

**Förklaring:**  
- Skriptet låter användaren välja både mappnamn och filnamn, vilket gör det mer interaktivt.

**Utmaning:** Prova att använda filnamn med filändelsen `.txt`.

---

## Övning 9: Skapa en mapp med ett förutbestämt prefix  
**Mål:** Använd en variabel med ett prefix och skapa filer med liknande namn.

**Steg för steg:**

1. **Skapa ett skript** `prefix_script.sh`:
   ```bash
   nano prefix_script.sh
   ```
2. **Skriv in koden:**
   ```bash
   #!/bin/bash
   mapp="ProjektMapp"
   prefix="dokument"

   echo "Skapar mappen $mapp..."
   mkdir $mapp

   echo "Skapar tre filer med prefixet $prefix..."
   touch $mapp/${prefix}1.txt
   touch $mapp/${prefix}2.txt
   touch $mapp/${prefix}3.txt

   echo "Filerna har skapats i mappen $mapp."
   ```
3. **Spara, gör körbar** och **kör skriptet**.

**Förklaring:**  
- Genom att använda `${prefix}` kan du enkelt ändra filnamnens inledning.

**Utmaning:** Byt ut prefixet mot något annat, t.ex. *rapport*.

---

## Övning 10: Skapa en undermapp och filer inuti den  
**Mål:** Lär dig att skapa en mapp inuti en annan mapp.

**Steg för steg:**

1. **Skapa ett skript** `undermapp.sh`:
   ```bash
   nano undermapp.sh
   ```
2. **Skriv in koden:**
   ```bash
   #!/bin/bash
   echo "Ange huvudmappens namn:"
   read huvudmapp
   mkdir $huvudmapp

   echo "Skapar en undermapp som heter InomMapp i $huvudmapp..."
   mkdir $huvudmapp/InomMapp

   echo "Skapar två filer i undermappen InomMapp..."
   touch $huvudmapp/InomMapp/fil1.txt
   touch $huvudmapp/InomMapp/fil2.txt

   echo "Filerna har skapats i $huvudmapp/InomMapp."
   ```
3. **Spara, gör körbar** och **kör skriptet**.

**Förklaring:**  
- Du skapar en undermapp genom att ange sökvägen `$huvudmapp/InomMapp`.

**Utmaning:** Skapa en extra fil i undermappen.

---

## Övning 11: Skapa två separata mappar med egna filer  
**Mål:** Skapa två mappar och lägga en fil i varje mapp.

**Steg för steg:**

1. **Skapa ett skript** `två_mappar.sh`:
   ```bash
   nano två_mappar.sh
   ```
2. **Skriv in koden:**
   ```bash
   #!/bin/bash
   echo "Skapar mappen MappA..."
   mkdir MappA
   touch MappA/filA.txt

   echo "Skapar mappen MappB..."
   mkdir MappB
   touch MappB/filB.txt

   echo "Mapparna MappA och MappB med sina filer har skapats."
   ```
3. **Spara, gör körbar** och **kör skriptet**.

**Förklaring:**  
- Varje `mkdir` skapar en separat mapp, och `touch` skapar en fil i respektive mapp.

**Utmaning:** Lägg till fler `echo`-meddelanden för att beskriva varje steg.

---

## Övning 12: Skriv text i en fil direkt vid skapandet  
**Mål:** Skapa en fil och skriv en rad text med `echo` och omdirigering (`>`).

**Steg för steg:**

1. **Skapa ett skript** `skriv_text.sh`:
   ```bash
   nano skriv_text.sh
   ```
2. **Skriv in koden:**
   ```bash
   #!/bin/bash
   filnamn="hälsning.txt"
   echo "Skapar filen $filnamn..."
   rm -f $filnamn
   echo "Hej! Välkommen till mitt skript." > $filnamn
   echo "Filen $filnamn har skapats med en hälsning."
   ```
3. **Spara, gör körbar** och **kör skriptet**.

**Förklaring:**  
- `>` skriver över filen med den nya texten.

**Utmaning:** Ändra texten som skrivs in.

---

## Övning 13: Skapa en fil med flera rader text  
**Mål:** Använd `>>` för att lägga till flera rader i en fil.

**Steg för steg:**

1. **Skapa ett skript** `flera_rader.sh`:
   ```bash
   nano flera_rader.sh
   ```
2. **Skriv in koden:**
   ```bash
   #!/bin/bash
   filnamn="berättelse.txt"
   rm -f $filnamn
   echo "Det var en gång..." > $filnamn
   echo "i en värld av Bash-skript." >> $filnamn
   echo "Äventyret började där." >> $filnamn
   echo "Filen $filnamn har skapats med flera rader text."
   ```
3. **Spara, gör körbar** och **kör skriptet**.

**Förklaring:**  
- `>>` lägger till text utan att ta bort tidigare innehåll.

**Utmaning:** Lägg till en extra rad med en egen berättelse.

---

## Övning 14: Visa innehållet i en fil med `cat`  
**Mål:** Lär dig att visa en fils innehåll i terminalen.

**Steg för steg:**

1. **Skapa ett skript** `visa_innehåll.sh`:
   ```bash
   nano visa_innehåll.sh
   ```
2. **Skriv in koden:**
   ```bash
   #!/bin/bash
   filnamn="berättelse.txt"
   echo "Innehållet i filen $filnamn är:"
   cat $filnamn
   ```
3. **Spara, gör körbar** och **kör skriptet**.

**Förklaring:**  
- `cat` visar hela innehållet i filen direkt i terminalen.

**Utmaning:** Kombinera med tidigare övningar så att du först skapar filen och sedan visar innehållet.

---

## Övning 15: Kombinera skapande av mapp och fil med text  
**Mål:** Skapa en mapp, skapa en fil i den och skriv in en hälsning.

**Steg för steg:**

1. **Skapa ett skript** `kombinerat.sh`:
   ```bash
   nano kombinerat.sh
   ```
2. **Skriv in koden:**
   ```bash
   #!/bin/bash
   mapp="HälsningsMapp"
   fil="hälsning.txt"

   echo "Skapar mappen $mapp..."
   mkdir $mapp

   echo "Skriver en hälsning i filen $fil..."
   echo "Hej! Hoppas du har en fantastisk dag!" > $mapp/$fil

   echo "Filen $fil har skapats i mappen $mapp med din hälsning."
   ```
3. **Spara, gör körbar** och **kör skriptet**.

**Förklaring:**  
- Denna övning kombinerar mapp- och filskapande med att skriva in text.

**Utmaning:** Ändra hälsningstexten och filnamnet efter eget tycke.

---

## Övning 16: Låt användaren bestämma både mapp- och filnamn  
**Mål:** Använd `read` för att göra skriptet interaktivt.

**Steg för steg:**

1. **Skapa ett skript** `interaktivt.sh`:
   ```bash
   nano interaktivt.sh
   ```
2. **Skriv in koden:**
   ```bash
   #!/bin/bash
   echo "Ange mappnamn:"
   read mappnamn
   echo "Ange filnamn (t.ex. hälsning.txt):"
   read filnamn

   rm -f $mappnamn/$filnamn 2>/dev/null
   mkdir -p $mappnamn

   echo "Hej! Detta är en interaktiv hälsning." > $mappnamn/$filnamn

   echo "Filen $filnamn har skapats i mappen $mappnamn."
   echo "Innehållet i filen är:"
   cat $mappnamn/$filnamn
   ```
3. **Spara, gör körbar** och **kör skriptet**.

**Förklaring:**  
- Skriptet låter användaren välja namn på mapp och fil samt visar innehållet efteråt.

**Utmaning:** Lägg till fler `echo`-meddelanden för att tydliggöra processen.

---

## Övning 17: Skapa en mapp med ett namn som innehåller mellanslag  
**Mål:** Lära sig hantera mappnamn med mellanslag genom att använda citationstecken.

**Steg för steg:**

1. **Skapa ett skript** `mellanslag.sh`:
   ```bash
   nano mellanslag.sh
   ```
2. **Skriv in koden:**
   ```bash
   #!/bin/bash
   echo "Skapar en mapp med namnet 'Min Fina Mapp'..."
   mkdir "Min Fina Mapp"

   echo "Skapar en fil i mappen 'Min Fina Mapp'..."
   touch "Min Fina Mapp/fil1.txt"

   echo "Filen har skapats i mappen 'Min Fina Mapp'."
   ```
3. **Spara, gör körbar** och **kör skriptet**.

**Förklaring:**  
- Genom att sätta mapp- och filnamn inom citationstecken hanterar du mellanslag i namnen.

**Utmaning:** Skapa en annan mapp med ett längre namn med mellanslag.

---

## Övning 18: Skapa en mapp och kopiera en fil in i den  
**Mål:** Lära sig att kopiera en fil med kommandot `cp`.

**Steg för steg:**

1. **Skapa ett skript** `kopiera_fil.sh`:
   ```bash
   nano kopiera_fil.sh
   ```
2. **Skriv in koden:**
   ```bash
   #!/bin/bash
   echo "Skapar mappen KopieradMapp..."
   mkdir KopieradMapp

   echo "Skapar filen original.txt med en hälsning..."
   echo "Detta är originalfilen." > original.txt

   echo "Kopierar original.txt till mappen KopieradMapp..."
   cp original.txt KopieradMapp/

   echo "Innehållet i KopieradMapp:"
   ls KopieradMapp
   ```
3. **Spara, gör körbar** och **kör skriptet**.

**Förklaring:**  
- `cp` kopierar en fil från en plats till en annan.

**Utmaning:** Visa innehållet i den kopierade filen med `cat`.

---

## Övning 19: Skapa en mapp och skapa två filer med olika innehåll  
**Mål:** Lära sig att skriva olika texter i olika filer.

**Steg för steg:**

1. **Skapa ett skript** `olika_innehall.sh`:
   ```bash
   nano olika_innehall.sh
   ```
2. **Skriv in koden:**
   ```bash
   #!/bin/bash
   mapp="VariationMapp"
   mkdir $mapp

   echo "Detta är den första filens innehåll." > $mapp/fil1.txt
   echo "Detta är den andra filens annorlunda innehåll." > $mapp/fil2.txt

   echo "Filerna har skapats i mappen $mapp."
   echo "Innehållet i fil1.txt:"
   cat $mapp/fil1.txt
   echo "Innehållet i fil2.txt:"
   cat $mapp/fil2.txt
   ```
3. **Spara, gör körbar** och **kör skriptet**.

**Förklaring:**  
- Varje fil får sitt eget innehåll med `>`.

**Utmaning:** Ändra texterna och filnamnen efter eget tycke.

---

## Övning 20: Avslutande utmaning – Kombinera allt i ett interaktivt skript  
**Mål:** Skapa ett skript som låter användaren ange mappnamn och tre filnamn, skapar mappen, skapar filerna med förutbestämt innehåll, och visar sedan mappens innehåll.

**Steg för steg:**

1. **Skapa ett skript** `fullt_interaktivt.sh`:
   ```bash
   nano fullt_interaktivt.sh
   ```
2. **Skriv in koden:**
   ```bash
   #!/bin/bash
   echo "Ange mappnamn:"
   read mappnamn

   echo "Ange namn för fil 1 (t.ex. fil1.txt):"
   read fil1
   echo "Ange namn för fil 2 (t.ex. fil2.txt):"
   read fil2
   echo "Ange namn för fil 3 (t.ex. fil3.txt):"
   read fil3

   rm -f $mappnamn/$fil1 $mappnamn/$fil2 $mappnamn/$fil3 2>/dev/null
   mkdir -p $mappnamn

   echo "Detta är innehållet i $fil1." > $mappnamn/$fil1
   echo "Detta är innehållet i $fil2." > $mappnamn/$fil2
   echo "Detta är innehållet i $fil3." > $mappnamn/$fil3

   echo "Filerna har skapats i mappen $mappnamn. Här är innehållet:"
   ls $mappnamn
   echo "Visa innehållet i filerna:"
   cat $mappnamn/$fil1
   echo "-----"
   cat $mappnamn/$fil2
   echo "-----"
   cat $mappnamn/$fil3
   ```
3. **Spara, gör körbar** och **kör skriptet**:
   ```bash
   chmod +x fullt_interaktivt.sh
   ./fullt_interaktivt.sh
   ```

**Förklaring:**  
- Detta skript kombinerar användarinput, mappskapande, filskapande med innehåll, och visar resultatet.  
- Det låter användaren anpassa både mapp- och filnamn.

**Utmaning:** Experimentera med att lägga in ytterligare rader i filerna genom att använda `>>` istället för `>`.
