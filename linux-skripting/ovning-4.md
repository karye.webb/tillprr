# Flytta och kopiera filer med ett skript

Målet med denna övning är att skapa ett skript som flyttar och kopierar filer mellan mappar. Du kommer att använda dig av `mv` och `cp` för att utföra dessa operationer.

> **Tips innan du börjar:**  
> - Öppna terminalen (t.ex. WSL Ubuntu) och gå till din hemkatalog med:  
>   ```bash
>   cd ~
>   ```  
> - Använd en textredigerare (t.ex. `nano`) för att skapa och redigera dina skript.  
> - Kom ihåg att spara dina filer, göra dem körbara med `chmod +x filnamn.sh` och sedan köra dem med `./filnamn.sh`.

---

# Övning 4: Flytta och kopiera filer med ett skript

### Övning 4.1: Skapa mappar och en testfil
**Mål:** Lär dig att skapa mappar med `mkdir` och en testfil med `touch`.

**Steg för steg:**
1. Öppna terminalen och gå till din hemkatalog:
   ```bash
   cd ~
   ```
2. Skapa ett skript med namnet `flytta_filer_01.sh`:
   ```bash
   nano flytta_filer_01.sh
   ```
3. Skriv in följande kod:
   ```bash
   #!/bin/bash
   echo "Skapar mapparna Mapp1 och Mapp2..."
   mkdir -p Mapp1
   mkdir -p Mapp2

   echo "Skapar testfilen i Mapp1..."
   touch Mapp1/testfil.txt

   echo "Klart! Mapp1 och Mapp2 har skapats med testfilen."
   ```
4. Spara, gör skriptet körbart och kör det:
   ```bash
   chmod +x flytta_filer_01.sh
   ./flytta_filer_01.sh
   ```

**Förklaring:**  
- `mkdir -p` skapar en mapp (och undviker fel om mappen redan finns).  
- `touch` skapar en tom fil.

**Utmaning:** Byt ut mappnamnen mot egna namn, t.ex. "SkolMapp" och "HemMapp".

---

### Övning 4.2: Kopiera en fil med `cp`
**Mål:** Kopiera en fil från en mapp till en annan.

**Steg för steg:**
1. Skapa ett nytt skript, t.ex. `flytta_filer_02.sh`:
   ```bash
   nano flytta_filer_02.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mkdir -p Mapp1
   mkdir -p Mapp2
   touch Mapp1/testfil.txt

   echo "Kopierar testfilen från Mapp1 till Mapp2..."
   cp Mapp1/testfil.txt Mapp2/

   echo "Filen har kopierats. Kolla innehållet i Mapp2 med 'ls Mapp2'."
   ```
3. Spara, gör körbar och kör skriptet:
   ```bash
   chmod +x flytta_filer_02.sh
   ./flytta_filer_02.sh
   ```

**Förklaring:**  
- `cp` kopierar filen från källan till destinationen.

**Utmaning:** Prova att använda `ls Mapp2` direkt i skriptet för att lista filen efter kopiering.

---

### Övning 4.3: Flytta och byt namn på en fil med `mv`
**Mål:** Flytta en fil och ändra dess namn.

**Steg för steg:**
1. Skapa ett skript, t.ex. `flytta_filer_03.sh`:
   ```bash
   nano flytta_filer_03.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mkdir -p Mapp2
   touch Mapp2/testfil.txt

   echo "Flyttar filen och byter namn på den..."
   mv Mapp2/testfil.txt Mapp2/ny_fil.txt

   echo "Filen har flyttats och bytt namn. Kontrollera Mapp2 med 'ls Mapp2'."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `mv` används både för att flytta och byta namn på filer.

**Utmaning:** Försök byta ut "ny_fil.txt" mot ett annat namn.

---

### Övning 4.4: Kopiera flera filer med jokertecken
**Mål:** Lär dig att kopiera alla filer med en viss filändelse.

**Steg för steg:**
1. Skapa ett skript, t.ex. `flytta_filer_04.sh`:
   ```bash
   nano flytta_filer_04.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mkdir -p Mapp1
   mkdir -p Mapp2

   # Skapar tre testfiler med .txt
   touch Mapp1/fil1.txt
   touch Mapp1/fil2.txt
   touch Mapp1/fil3.txt

   echo "Kopierar alla .txt-filer från Mapp1 till Mapp2..."
   cp Mapp1/*.txt Mapp2/

   echo "Alla .txt-filer har kopierats till Mapp2."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Jokertecknet `*` används för att matcha alla filer med filändelsen `.txt`.

**Utmaning:** Prova att skapa även filer med en annan ändelse och kopiera bara `.txt`-filer.

---

### Övning 4.5: Flytta flera filer med `mv`
**Mål:** Flytta alla filer från en mapp till en annan.

**Steg för steg:**
1. Skapa ett skript, t.ex. `flytta_filer_05.sh`:
   ```bash
   nano flytta_filer_05.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mkdir -p Mapp1
   mkdir -p Mapp2

   # Skapa testfiler i Mapp1
   touch Mapp1/fil1.txt
   touch Mapp1/fil2.txt
   touch Mapp1/fil3.txt

   echo "Flyttar alla filer från Mapp1 till Mapp2..."
   mv Mapp1/* Mapp2/

   echo "Alla filer har flyttats. Kontrollera Mapp2 med 'ls Mapp2'."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `mv Mapp1/* Mapp2/` flyttar allt innehåll från Mapp1 till Mapp2.

**Utmaning:** Testa att flytta bara filer med en specifik ändelse (t.ex. `mv Mapp1/*.txt Mapp2/`).

---

### Övning 4.6: Använd variabler för mappar och filnamn
**Mål:** Göra skriptet mer flexibelt med variabler.

**Steg för steg:**
1. Skapa ett skript, t.ex. `flytta_filer_06.sh`:
   ```bash
   nano flytta_filer_06.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mappKalla="Mapp1"
   mappMotta="Mapp2"
   fil="testfil.txt"

   echo "Skapar mapparna $mappKalla och $mappMotta..."
   mkdir -p $mappKalla
   mkdir -p $mappMotta

   echo "Skapar filen $fil i $mappKalla..."
   touch $mappKalla/$fil

   echo "Kopierar $fil från $mappKalla till $mappMotta..."
   cp $mappKalla/$fil $mappMotta/

   echo "Flyttar och byter namn på filen i $mappMotta..."
   mv $mappMotta/$fil $mappMotta/nytt_namn.txt

   echo "Klart! Kolla innehållet i $mappKalla och $mappMotta."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Variabler gör det enklare att ändra mapp- och filnamn på ett ställe.

**Utmaning:** Byt ut variabelvärdena och se hur utskriften ändras.

---

### Övning 4.7: Låt användaren välja mappnamn med `read`
**Mål:** Gör skriptet interaktivt så att användaren anger mappnamn.

**Steg för steg:**
1. Skapa ett skript, t.ex. `flytta_filer_07.sh`:
   ```bash
   nano flytta_filer_07.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Ange källmappens namn:"
   read mappKalla
   echo "Ange målmappens namn:"
   read mappMotta

   mkdir -p $mappKalla
   mkdir -p $mappMotta

   echo "Skapar testfilen i $mappKalla..."
   touch $mappKalla/testfil.txt

   echo "Kopierar testfilen till $mappMotta..."
   cp $mappKalla/testfil.txt $mappMotta/

   echo "Fil kopierad! Nu byter vi namn på filen i $mappMotta..."
   mv $mappMotta/testfil.txt $mappMotta/nytt_namn.txt

   echo "Klart! Kontrollera mapparna."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `read` tar emot användarens inmatning och sparar den i variabler.

**Utmaning:** Lägg till en fråga om användaren vill radera originalfilen efter kopiering.

---

### Övning 4.8: Kopiera filer och radera originalet (simulera "klipp ut" med `rm`)
**Mål:** Använd `cp` följt av `rm` för att flytta (klippa ut) filer.

**Steg för steg:**
1. Skapa ett skript, t.ex. `flytta_och_radera.sh`:
   ```bash
   nano flytta_och_radera.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mkdir -p Mapp1
   mkdir -p Mapp2
   touch Mapp1/testfil.txt

   echo "Kopierar testfilen från Mapp1 till Mapp2..."
   cp Mapp1/testfil.txt Mapp2/
   echo "Raderar originalfilen i Mapp1..."
   rm Mapp1/testfil.txt

   echo "Testfilen har flyttats (kopierats och sedan raderats)!"
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Först kopieras filen med `cp` och sedan tas originalet bort med `rm`.

**Utmaning:** Lägg till en extra `echo`-rad som bekräftar att filen inte längre finns i Mapp1.

---

### Övning 4.9: Kopiera en hel mapp med `cp -r`
**Mål:** Lära sig att kopiera en mapp med allt innehåll med alternativet `-r`.

**Steg för steg:**
1. Skapa ett skript, t.ex. `kopiera_mapp.sh`:
   ```bash
   nano kopiera_mapp.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mkdir -p KallaMapp
   mkdir -p KopiaMapp
   touch KallaMapp/fil1.txt
   touch KallaMapp/fil2.txt

   echo "Kopierar hela mappen KallaMapp till KopiaMapp..."
   cp -r KallaMapp KopiaMapp/

   echo "Mappen har kopierats. Kolla i KopiaMapp/KallaMapp."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `cp -r` kopierar en mapp rekursivt, dvs. med allt innehåll.

**Utmaning:** Testa att lägga till en undermapp i KallaMapp innan kopiering.

---

### Övning 4.10: Flytta en hel mapp med `mv`
**Mål:** Flytta en mapp med all dess innehåll med `mv`.

**Steg för steg:**
1. Skapa ett skript, t.ex. `flytta_mapp.sh`:
   ```bash
   nano flytta_mapp.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mkdir -p UrsprungMapp
   touch UrsprungMapp/fil.txt

   echo "Flyttar mappen UrsprungMapp och byter namn till NyMapp..."
   mv UrsprungMapp NyMapp

   echo "Mappen har flyttats. Kontrollera med 'ls'."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `mv` flyttar hela mappen och kan även byta namn på den.

**Utmaning:** Försök att flytta mappen tillbaka till det ursprungliga namnet.

---

### Övning 4.11: Byt namn på flera filer med `mv`
**Mål:** Använd `mv` för att byta namn på flera filer individuellt.

**Steg för steg:**
1. Skapa ett skript, t.ex. `byta_namn.sh`:
   ```bash
   nano byta_namn.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mkdir -p Mapp1
   touch Mapp1/fil1.txt
   touch Mapp1/fil2.txt

   echo "Byter namn på filerna i Mapp1..."
   mv Mapp1/fil1.txt Mapp1/ny_fil1.txt
   mv Mapp1/fil2.txt Mapp1/ny_fil2.txt

   echo "Filerna har bytt namn. Lista Mapp1 med 'ls Mapp1'."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Varje `mv`-kommando byter namn på en enskild fil.

**Utmaning:** Lägg till en extra fil och byt namn på den också.

---

### Övning 4.12: Kopiera filer med specifik filändelse
**Mål:** Kopiera endast filer med en viss ändelse (t.ex. .txt).

**Steg för steg:**
1. Skapa ett skript, t.ex. `kopiera_txt.sh`:
   ```bash
   nano kopiera_txt.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mkdir -p KallaMapp
   mkdir -p MottagarMapp

   # Skapa filer med olika ändelser
   touch KallaMapp/fil1.txt
   touch KallaMapp/fil2.txt
   touch KallaMapp/fil3.doc

   echo "Kopierar bara .txt-filer från KallaMapp till MottagarMapp..."
   cp KallaMapp/*.txt MottagarMapp/

   echo "Kolla MottagarMapp med 'ls MottagarMapp'."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Endast filer som matchar mönstret `*.txt` kopieras.

**Utmaning:** Testa med att skapa fler filer med olika ändelser.

---

### Övning 4.13: Flytta filer med en loop (utan avancerade loopar)
**Mål:** Kopiera och flytta flera filer genom att skriva ut flera `mv`-kommandon manuellt.

**Steg för steg:**
1. Skapa ett skript, t.ex. `manuell_flytt.sh`:
   ```bash
   nano manuell_flytt.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mkdir -p MappA
   mkdir -p MappB

   touch MappA/fil1.txt
   touch MappA/fil2.txt
   touch MappA/fil3.txt

   echo "Flyttar fil1.txt..."
   mv MappA/fil1.txt MappB/
   echo "Flyttar fil2.txt..."
   mv MappA/fil2.txt MappB/
   echo "Flyttar fil3.txt..."
   mv MappA/fil3.txt MappB/

   echo "Alla filer har flyttats till MappB."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Här skrivs varje flyttkommando ut separat utan loopar – bra för nybörjare.

**Utmaning:** Prova att lägga till fler filer och flytta dem på samma sätt.

---

### Övning 4.14: Kopiera och flytta filer med bekräftelse
**Mål:** Lägg till extra `echo`-meddelanden för att bekräfta varje steg.

**Steg för steg:**
1. Skapa ett skript, t.ex. `bekrafta.sh`:
   ```bash
   nano bekrafta.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mkdir -p Mapp1
   mkdir -p Mapp2
   touch Mapp1/testfil.txt

   echo "Steg 1: Kopierar testfilen från Mapp1 till Mapp2..."
   cp Mapp1/testfil.txt Mapp2/
   echo "Steg 2: Filen har kopierats. Nu byter vi namn på den."
   mv Mapp2/testfil.txt Mapp2/ny_fil.txt
   echo "Alla steg är klara. Kontrollera Mapp1 och Mapp2."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Extra meddelanden gör att du tydligt ser vad som händer.

**Utmaning:** Lägg till en bekräftelse efter varje filflytt.

---

### Övning 4.15: Kopiera filer med globbing och sedan flytta dem
**Mål:** Kopiera alla filer med ett visst mönster och sedan flytta dem till en annan mapp.

**Steg för steg:**
1. Skapa ett skript, t.ex. `kopiera_och_flytta.sh`:
   ```bash
   nano kopiera_och_flytta.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mkdir -p KallaMapp
   mkdir -p DestinationMapp

   touch KallaMapp/rapport1.txt
   touch KallaMapp/rapport2.txt
   touch KallaMapp/rapport3.txt
   touch KallaMapp/anteckningar.doc

   echo "Kopierar alla .txt-filer..."
   cp KallaMapp/*.txt DestinationMapp/

   echo "Flyttar de kopierade .txt-filerna från DestinationMapp och byter namn..."
   mv DestinationMapp/rapport1.txt DestinationMapp/ny_rapport1.txt
   mv DestinationMapp/rapport2.txt DestinationMapp/ny_rapport2.txt
   mv DestinationMapp/rapport3.txt DestinationMapp/ny_rapport3.txt

   echo "Processen är klar!"
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Först kopieras alla .txt-filer och sedan flyttas och byts namn på dem.

**Utmaning:** Lägg till en rad som raderar originalfilerna i KallaMapp efter kopieringen.

---

### Övning 4.16: Radera filer efter kopiering (alternativ till flytt)
**Mål:** Kopiera filer med `cp` och ta bort originalet med `rm`.

**Steg för steg:**
1. Skapa ett skript, t.ex. `kopiera_och_radera.sh`:
   ```bash
   nano kopiera_och_radera.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mkdir -p KallaMapp
   mkdir -p DestinationMapp

   touch KallaMapp/testfil.txt

   echo "Kopierar testfilen..."
   cp KallaMapp/testfil.txt DestinationMapp/
   echo "Tar bort originalfilen..."
   rm KallaMapp/testfil.txt

   echo "Filen finns nu bara i DestinationMapp."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Detta simulerar "klipp ut" genom att kopiera med `cp` och sedan ta bort originalet med `rm`.

**Utmaning:** Lägg till en kontroll med `ls` för att verifiera att filen inte längre finns i KallaMapp.

---

### Övning 4.17: Hantera filnamn med mellanslag
**Mål:** Lära sig att hantera filnamn som innehåller mellanslag med citationstecken.

**Steg för steg:**
1. Skapa ett skript, t.ex. `mellanslag_namn.sh`:
   ```bash
   nano mellanslag_namn.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Skapar en mapp med ett namn med mellanslag..."
   mkdir "Min Fina Mapp"

   echo "Skapar en fil med mellanslag i namnet..."
   touch "Min Fina Mapp/fil med mellanslag.txt"

   echo "Filen har skapats i 'Min Fina Mapp'."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Använd citationstecken runt namn med mellanslag för att hantera dem korrekt.

**Utmaning:** Skapa ytterligare en fil med ett annat namn med mellanslag.

---

### Övning 4.18: Kopiera en fil och byta namn med datum
**Mål:** Lägg till en enkel tidsstämpel (en fast text) i filnamnet vid kopiering.

**Steg för steg:**
1. Skapa ett skript, t.ex. `datum_namn.sh`:
   ```bash
   nano datum_namn.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mkdir -p Mapp1
   mkdir -p Mapp2
   touch Mapp1/testfil.txt

   echo "Kopierar filen och lägger till '_2025' i filnamnet..."
   cp Mapp1/testfil.txt Mapp2/testfil_2025.txt

   echo "Fil kopierad till Mapp2 med nytt namn:"
   ls Mapp2
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Här simuleras en tidsstämpel genom att lägga till en fast sträng i filnamnet.

**Utmaning:** Försök att ändra den fasta texten till något annat, t.ex. "_v1".

---

### Övning 4.19: Kopiera filer med olika ändelser och flytta bara en typ
**Mål:** Kopiera endast filer med ändelsen .txt från en mapp med blandade filer.

**Steg för steg:**
1. Skapa ett skript, t.ex. `filter_copy.sh`:
   ```bash
   nano filter_copy.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   mkdir -p BlandadMapp
   mkdir -p TxtMapp

   touch BlandadMapp/fil1.txt
   touch BlandadMapp/fil2.doc
   touch BlandadMapp/fil3.txt
   touch BlandadMapp/fil4.pdf

   echo "Kopierar endast .txt-filer från BlandadMapp till TxtMapp..."
   cp BlandadMapp/*.txt TxtMapp/

   echo "Kopierade filer i TxtMapp:"
   ls TxtMapp
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Endast de filer som matchar `*.txt` kopieras.

**Utmaning:** Skapa ytterligare filer med olika ändelser och testa igen.

---

### Övning 4.20: Avslutande interaktivt skript – Välj mellan kopiera och flytta
**Mål:** Skapa ett interaktivt skript där användaren får välja om de vill kopiera eller flytta filer.

**Steg för steg:**
1. Skapa ett skript, t.ex. `interaktiv_flytta_kopiera.sh`:
   ```bash
   nano interaktiv_flytta_kopiera.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Ange källmapp (där filerna finns):"
   read kalla
   echo "Ange målmapp (dit filerna ska gå):"
   read mottagare
   echo "Ange filnamn (ex. testfil.txt):"
   read filnamn

   mkdir -p $kalla
   mkdir -p $mottagare

   # Skapa en testfil i källmappen om den inte finns
   if [ ! -f $kalla/$filnamn ]; then
       touch $kalla/$filnamn
       echo "Testfilen $filnamn skapades i $kalla."
   fi

   echo "Vill du (1) kopiera eller (2) flytta filen? (Ange 1 eller 2)"
   read val

   if [ "$val" = "1" ]; then
       cp $kalla/$filnamn $mottagare/
       echo "Filen har kopierats från $kalla till $mottagare."
   else
       mv $kalla/$filnamn $mottagare/
       echo "Filen har flyttats från $kalla till $mottagare."
   fi

   echo "Innehållet i $mottagare:"
   ls $mottagare
   ```
3. Spara, gör körbar och kör skriptet:
   ```bash
   chmod +x interaktiv_flytta_kopiera.sh
   ./interaktiv_flytta_kopiera.sh
   ```

**Förklaring:**  
- Detta skript låter användaren välja mellan att kopiera eller flytta en fil.  
- Det visar även hur man hanterar användarinput med `read` och en enkel if-sats (även om if-satsen är introducerad på ett grundläggande sätt).

**Utmaning:** Experimentera med att lägga till fler alternativ, t.ex. att byta namn på filen efter flytten.
