---
description: >-
  Anslut en dator och börja skriva program för din Raspberry Pi Pico med hjälp
  av MicroPython-språket
---

# Intro till MicroPython

![](<../.gitbook/assets/image (34).png>)

## Programmera med MicroPython

Programmeringsspråket **Python** - uppkallat efter den berömda komeditruppen Monty Python, snarare än ormen - har vuxit till att bli en av de mest populära i världen. Dess popularitet betyder dock inte att det inte finns några förbättringar - särskilt om du arbetar med en mikrokontroller. Programmeringsspråket Python utvecklades för datorsystem som stationära datorer, bärbara datorer och servrar. Mikrokontrollerkort som Raspberry Pi Pico är mindre, enklare och med betydligt mindre minne - vilket betyder att de inte kan köra samma Python-språk som sina större motsvarigheter.

Det är där MicroPython kommer in. MicroPython, som ursprungligen utvecklades av **Damien George** och släpptes första gången 2014, är ett Python-kompatibelt programmeringsspråk som utvecklats speciellt för mikrokontroller. Den innehåller många av funktionerna i vanliga Python, samtidigt som den lägger till en rad nya som är utformade för att dra nytta av de faciliteter som finns på Raspberry Pi Pico och andra mikrokontrollerkort.

Om du har programmerat med Python tidigare kommer MicroPython kännas bekant. Om inte, oroa dig inte: det är ett vänligt språk att lära sig!

### Ditt första MicroPython-program: Hej, världen!

Skapa ett nytt program genom att klicka på **Nytt** i Thony, spara programmet och kalla det **Hello\_World.py:**

![Figur 2-1](<../.gitbook/assets/image (38).png>)

Skriv följande instruktion:

```python
print("Hej, världen!")
```

När du trycker på **ENTER** den här gången händer ingenting - förutom att du får en ny, tom rad i. För att ditt program ska fungera måste du klicka på **Run** i Thony.

![](<../.gitbook/assets/image (40).png>)

Klicka på **Run** nu:

![Figur 2-3](<../.gitbook/assets/image (35).png>)

### Nästa steg: loopar och kodindrag

Ett MicroPython-program, precis som med ett vanligt Python-program, körs normalt topp-till-botten: det går igenom varje rad i tur och ordning innan det går vidare till nästa, precis som om du skrev dem i konsolen.

Ett program som bara går igenom en lista med instruktioner rad-för-rad skulle dock inte vara särskilt smart, så MicroPython, precis som Python, har sitt eget sätt att styra sekvensen där dess program körs: indrag.

Skapa ett nytt program genom att klicka på **Nytt** i Thony, spara programmet och kalla det **Indentation.py**. Du förlorar inte ditt befintliga program; istället skapar Thony en ny flik ovanför skriptområdet.

![](<../.gitbook/assets/image (36).png>)

Börja ditt program genom att skriva följande två rader:

```python
print("Loopen startar!")

for i in range(10):
```

Den första raden skriver ut ett enkelt meddelande till konsolen, precis som ditt Hello World-program. Den andra börjar en bestämd loop, som kommer att repetera en eller flera instruktioner ett visst antal gånger. En variabel, i, tilldelas loopen och kommer att börja med siffran 0 och arbeta uppåt mot, men aldrig nå, talet 10. Kolon-symbolen (:) berättar för MicroPython att loopen börjar på nästa rad.

Variabler är kraftfulla verktyg: som namnet antyder är variabler värden som kan förändras - eller variera - över tid och under kontroll av programmet. En variabel har två sidor: dess namn och datan som den lagrar. I fallet med din loop är variabelns namn **"i"** och dess data ställs in av intervallinstruktionen: den börjar på 0 och ökar med 1 varje gång loopen börjar om.

För att faktiskt inkludera en kodrad i loopen måste den indenteras, dvs flyttas in från vänster sida. Nästa rad börjar med en **TAB** som Thony kommer att ha lagt till automatiskt när du tryckte på **ENTER** efter rad 3. Skriv in det nu:

```python
print("Loopen startar!")

for i in range(10):
    print("Loop nummer ", i)
```

Denna indragning är hur MicroPython vet skillnaden mellan instruktioner **utanför loopen** och instruktioner **inuti loopen**: den indragna koden, som bildar insidan av loopen kallas för inkapslad.

Du kommer att märka att när du tryckte på **RETUR** i slutet av den tredje raden indenterar Thony automatiskt nästa rad; förutsatt att det skulle vara en del av loopen. För att ta bort denna indragning, tryck bara på **BACKSPACE** en gång innan du skriver den sista raden:

```python
print("Loopen startar!")

for i in range(10):
    print("Loop nummer ", i)

print("Loopen klar!")
```

Ditt fyrradiga program är nu klart. Den första raden sitter utanför loopen och körs bara en gång; den andra raden sätter upp loopen; den tredje sitter inuti loopen och kommer att köras en gång för varje gång loopen går; och den fjärde raden sitter utanför loopen igen.

Klicka på **Run**. Så här ser det ut i konsolen:

```bash
Loopen start!
loop nummer 0
Loop nummer 1
Loop nummer 2
Loop nummer 3
Loop nummer 4
Loop nummer 5
Loop nummer 6
Loop nummer 7
Loop nummer 8
Loop nummer 9
Loopen klar!
```

**Fel på indentering** är en av de vanligaste anledningarna till att ett program inte fungerar som du förväntat dig. När du letar efter problem i ett program, en process som kallas felsökning, ska du alltid dubbelkolla indenteringen särskilt när du börjar köra loopar i loopar.

### Oändliga loopar

MicroPython stöder också oändliga loopar, som körs utan slut. För att ändra ditt program från en bestämd loop till en oändlig loop, redigera rad 3 så att det står:

```python
print("Loopen startar!")

while True:
    print("Loop nummer ", i)

print("Loopen klar!")
```

Eftersom vi inte längre kommer att använda variabeln i, ändra rad 3 för att läsa:

```python
print("Loopen startar!")

while True:
    print("Loop nummer ")

print("Loopen klar!")
```

### Fördröjning

För att undvika program som körs för fort kommer vi också lägga till en kort fördröjning genom att importera **utime**-biblioteket i början och lägga en sekunds fördröjning till loopen (du lär dig mer om detta bibliotek i senare kapitel). Ditt program ska nu se ut så här:

```python
import utime
print("Loopen startar!")

while True:
    print("Loop snurrar!")
    utime.sleep(1)

print("Loopen klar!")
```

Klicka på **Run** igen, du ser "Loopen startar!" följt av en oändlig sträng av "Loop snurrar!". "Loopen klar!" kommer aldrig att skrivas ut, eftersom loopen inte har något slut: varje gång Python har skrivit ut "Loop snurrar!" går det tillbaka till början av loopen och skriver ut det igen.

Klicka på **Stop** i Thony för att säga till för programmet att stoppa vad det gör. Så kallas att avbryta programmet. Du kommer att se ett meddelande visas i konsolen och programmet kommer att sluta utan att någonsin nå rad 8.

![](<../.gitbook/assets/image (39).png>)

{% hint style="info" %}
**UTMANING: Loopa loopen**\
Kan du ändra loopen tillbaka till en bestämd loop igen? Kan du lägga till en andra bestämd loop till programmet? Hur skulle du lägga till en loop in a loop, och hur skulle du förvänta dig att det skulle fungera?
{% endhint %}

## Villkor och variabler

Variabler i MicroPython, som i alla programmeringsspråk, finns för mer än bara att styra loopar. Påbörja ett nytt program genom att klicka på **Ny** i Thony och skriv sedan följande i sidan:

```python
namn = input("Vad heter du?")
```

Klicka på **Spara** och ge det namnet **Name\_Test.py. Kör** sedan programmet och titta på vad som händer i konsolen: du kommer att bli ombedd om ditt namn. Skriv ditt namn i konsolen, följt av **ENTER**.\
Eftersom det är den enda instruktionen i ditt program kommer inget annat att hända.&#x20;

![](<../.gitbook/assets/image (42).png>)

Om du faktiskt vill göra någonting med den data du har placerat i variabeln behöver du fler rader i ditt program.

### Använda = och ==

{% hint style="warning" %}
Nyckeln till att använda variabler är att lära sig skillnaden mellan = och ==.\
Kom ihåg: = betyder 'gör denna variabel lika med detta värde', medan == betyder 'kontrollera om variabeln är lika med detta värde'.\
Att blanda ihop dem är ett säkert sätt att hamna med ett program som inte fungerar!
{% endhint %}

För att ditt program ska göra något användbart med namnet, lägg till ett villkorligt uttalande genom att skriva följande från rad 3 och framåt:

```python
namn = input("Vad heter du?")

if namn == "Clark Kent":
    print("Du är Superman!")
else:
    print("Du är inte Superman!")
```

Kom ihåg att när Thony ser att din kod måste indenteras kommer den att göra det automatiskt - men den vet inte när din kod måste sluta indenteras, så du måste ta bort **TAB** själv.

Klicka på **Run** och skriv ditt namn i konsolen. Om inte ditt namn råkar vara "Clark Kent" ser du meddelandet "Du är inte Superman!". Klicka på **Run** igen och skriv den här gången in namnet "Clark Kent" - se till att skriva det exakt som i programmet, med stort C och K. Den här gången känner programmet igen att du faktiskt är Superman.

![](<../.gitbook/assets/image (43).png>)

### Jämförelseoperatorer

Symbolen **==** berätta för Python att göra en direkt jämförelse mellan variabeln namn och texten du matar in. Om du arbetar med siffror finns det andra jämförelser du kan göra:  \
**>** för att se om ett tal är större än ett annat tal,  \
**<** för att se om det är mindre än,  \
**>=** för att se om det är större än eller lika med,  \
**<=** för att se om det är mindre än eller lika med.

Det finns också **!=** , Vilket betyder inte lika med - det är raka motsatsen till **==**. Dessa symboler är tekniskt kända som jämförelseoperatorer .

Jämförelseoperatorer kan också användas i loopar. Ta bort raderna 3 till 6 och skriv sedan följande i stället:

```python
namn = input("Vad heter du?")

while namn != "Clark Kent" :
    print("Du är inte Superman - försök igen!")
    namn = input("Vad heter du?")
print("Du är superman!")
```

{% hint style="info" %}
**UTMANING: LÄGG FLER FRÅGOR**\
Kan du ändra programmet för att ställa mer än en fråga, lagra svaren i flera variabler? Kan du göra en program som använder villkor och jämförelseoperatörer att skriva ut om ett antal som skrivits in av användaren är högre eller lägre än 5?
{% endhint %}

Klicka på **Run** igen. Den här gången, snarare än att sluta, kommer programmet att fortsätta att fråga efter ditt namn tills dess du bekräftar att du är Superman (Figur 2-9) ungefär som ett mycket enkelt lösenordskontroll. För att komma ur loopen, antingen skriver du "Clark Kent" i konsolen eller klickar på **Stop** i Thony.

![](<../.gitbook/assets/image (44).png>)

Grattis! Du vet nu hur du använder villkor och jämförelseoperatörer!
