# Prova tända extern LED

![Figur 4-3](<../../.gitbook/assets/image (27).png>)

## Anslut en extern LED

För att vidga våra experiment ytterligare kan vi ansluta en extern LED till en av mikrokontrollerens lediga pinnar, exempelvis **GP15**. Detta steg kräver lite extra hårdvaruinstallation, men öppnar upp för fler möjligheter.

När du har anslutit din externa LED till **GP15**, ändra din kod för att reflektera detta:

```python
# Inkludera nödvändigt bibliotek
from machine import Pin

# Den här gången ansluter vi vår LED till pin GP15
led = Pin(15, Pin.OUT)
# För att tända den externa LED
led.on()
```

## Få LED-en att blinka

Att få en LED att blinka är ett klassiskt första programmeringsprojekt. Det introducerar konceptet av tidfördröjning mellan aktioner och ger en visuell återkoppling på din kod.

### Inför fördröjning

För att skapa effekten av en blinkande LED, alternerar vi mellan att tända och släcka LED-en, med en paus emellan varje tillståndsändring.

```python
# Inkludera nödvändiga bibliotek för att kontrollera pinnar och hantera tid
from machine import Pin, utime

# Vi ansluter återigen vår LED till GP15
led = Pin(15, Pin.OUT)

# Tänd LED
led.on()

# Vänta 1 sekund innan nästa steg
utime.sleep(1)

# Släck LED
led.off()
```

### Skapa en kontinuerlig blinkande effekt

För att få LED-en att kontinuerligt blinka, omsluter vi vår kod i en oändlig loop. Detta innebär att processen att tända, vänta, och släcka kommer att upprepas tills programmet avslutas.

```python
# Importera nödvändiga bibliotek
from machine import Pin, utime

# Anslut din LED till GP15 som tidigare
led = Pin(15, Pin.OUT)

# Använd en while-loop för att kontinuerligt blinka LED-en
while True:
    # Tänd LED
    led.on()

    # Vänta 1 sekund
    utime.sleep(1)

    # Släck LED
    led.off()

    # Vänta återigen 1 sekund innan loopen börjar om
    utime.sleep(1)
```

Genom att följa dessa steg och experimentera med koden, får du en god förståelse för grundläggande programmering och interaktion med elektroniska komponenter.

## Uppgifter

1. **Ändra blinkfrekvensen:** Prova att ändra värdet i `utime.sleep()` för att justera blinkfrekvensen. Hur påverkar en kortare eller längre fördröjning upplevelsen av blinkandet?
1. **Använd flera LED-lampor:** Anslut flera LED-lampor till olika pinnar och skapa ett mönster där de blinkar i sekvens. Hur kan du organisera koden för att hantera flera LED-lampor samtidigt?