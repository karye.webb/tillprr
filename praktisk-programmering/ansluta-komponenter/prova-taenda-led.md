# Prova tända LED

![Figur 4-2](<../../.gitbook/assets/image (26).png>)

Denna guide är till för att hjälpa dig komma igång med programmering av mikrokontrollers genom ett enkelt och praktiskt exempel: att tända och släcka en LED. Vi börjar med den inbyggda LED som finns på GPIO-pinne 25 på din mikrokontroller. Senare kommer vi även att visa hur du kan ansluta och styra en extern LED.

## Tänd en inbyggd LED

För att förstå hur vi interagerar med mikrokontrollerens hårdvara, kommer vi att börja med ett grundläggande experiment: att tända den inbyggda LED-lampan. Detta ger oss en inblick i hur digitala signaler kan användas för att styra elektroniska komponenter.

```python
# Först, inkludera nödvändiga bibliotek för att kontrollera pinnar
from machine import Pin

# Vi initierar en Pin GP25, inställd på utgående signal
led = Pin(25, Pin.OUT)

# För att tända LED
led.on()
```

## Hur man släcker LED-en

För att släcka LED-en använder vi samma princip som när vi tänder den, men istället sätter vi pin-värdet till 0, vilket motsvarar en låg signal.

```python
# Återigen, inkludera biblioteket för att hantera pinnar
from machine import Pin

# Initiera samma pin som tidigare, GP25, för utgående signal
led = Pin(25, Pin.OUT)

# För att släcka LED
led.off()
```

## Få LED-en att blinka

Att få en LED att blinka är ett klassiskt första programmeringsprojekt. Det introducerar konceptet av tidfördröjning mellan aktioner och ger en visuell återkoppling på din kod.

### Inför fördröjning

För att skapa effekten av en blinkande LED, alternerar vi mellan att tända och släcka LED-en, med en paus emellan varje tillståndsändring.

```python
# Inkludera nödvändiga bibliotek för att kontrollera pinnar och hantera tid
from machine import Pin
import utime

# Vi ansluter återigen vår LED till GP25
led = Pin(25, Pin.OUT)

# Tänd LED
led.on()

# Vänta 1 sekund innan nästa steg
utime.sleep(1)

# Släck LED
led.off()
```

### Uppgifter

1. Skriv om koden så att LED-en blinkar med en kortare fördröjning, exempelvis 0.5 sekunder.
1. Hur kort kan du göra fördröjningen innan LED-en börjar blinka för snabbt för att du ska kunna se det?
1. Skriv om koden så att LED-en morseblinkar SOS (tre korta blinkningar, tre långa blinkningar, tre korta blinkningar): `...---...`