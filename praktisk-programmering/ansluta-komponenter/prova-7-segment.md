# Prova tända 7-segmentsdisplay

{% embed url="https://youtu.be/697oms_v9ys?si=sD-XsA6hutuqRTO3" %}

I denna övning kommer vi att ansluta en 7-segmentsdisplay till en Raspberry Pi Pico och skriva ett enkelt program för att tända olika segment.


## Material

För att genomföra denna övning behöver du följande material:

* 1 x Raspberry Pi Pico
* 1 x 7-segmentsdisplay
* 1 x 220 ohm motstånd
* 8 x ledningar

## 7-segmentsdisplay

En 7-segmentsdisplay har 8 pinnar som är kopplade till varje segment och en gemensam anod eller katod. Segmenten är numrerade från 0 till 7 enligt tabellen nedan:

| Segment | Pin |
| :---: | :---: |
| p | 12 |
| c | 13 |
| d | 14 |
| e | 15 |
| g | 16 |
| f | 17 |
| a | 18 |
| b | 19 |

![alt text](../../.gitbook/assets/image-10.png)

## Anslutningsschema

Anslut 7-segmentsdisplayen till Raspberry Pi Pico enligt anslutningsschemat nedan:

![alt text](../../.gitbook/assets/image-9.png)

## Program

För att tända olika segment på 7-segmentsdisplayen kan du använda följande program:

```python
# Importera bibliotek
from machine import Pin
Import utime

# Definiera pinnarna för segmenten
pin_p = Pin(12, Pin.OUT)  # Punkten
pin_c = Pin(13, Pin.OUT)  # Botten höger
pin_d = Pin(14, Pin.OUT)  # Bottom
pin_e = Pin(15, Pin.OUT)  # Botten vänster
pin_g = Pin(16, Pin.OUT)  # Mitten
pin_f = Pin(17, Pin.OUT)  # Toppen vänster
pin_a = Pin(18, Pin.OUT)  # Toppen
pin_b = Pin(19, Pin.OUT)  # Toppen höger

# Skriv ut siffra 1
pin_p.off()
pin_c.on()
pin_d.off()
pin_e.off()
pin_g.off()
pin_f.off()
pin_a.off()
pin_b.on()
```

## Uppgifter

1. Visa text "S O S", en bokstav i taget.\
  Tips: använd `utime.sleep()` för att vänta mellan varje bokstav.
1. Lägg till en knapp och ändra programmet så att displayen visar antalet gånger knappen har tryckts.\
  Tips: Använd en variabel för att räkna antalet tryck på knappen och uppdatera displayen i loopen.