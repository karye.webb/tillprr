# Ansluta komponenter

Raspberry Pi Pico, med sin **RP2040** mikrokontroller, är utformad med industriell programmering i åtanke. Dess många allmänna in-/utgångar (GPIO) gör att den kan prata med en rad olika komponenter, så att du kan bygga upp projekt, från belysning av lysdioder till inspelning av data om din omgivning.

industriell programmering är inte svårare att lära sig än traditionell dator: om du kunde följa exemplen i kapitel 2 kommer du att kunna bygga dina egna kretsar och programmera dem för att göra dina bud.

## Ditt första fysiska datorprogram: Hej, LED!

Precis som att skriva ut "Hej, världen" på skärmen är ett fantastiskt första steg för att lära sig ett programmeringsspråk, är att en LED-lampa tänds den traditionella introduktionen till inlärning av fysiska datorer. Du kan också komma igång utan några extra komponenter: din Raspberry Pi Pico har en liten lysdiod, känd som en ytmonterad enhet (SMD), ovanpå.

Börja med att hitta lysdioden: det är den lilla rektangulära komponenten till vänster om mikro-USB porten högst upp på kortet (Figur 4-1), märkt med en etikett med texten 'LED'. Den här lilla lysdioden fungerar precis som alla andra: när den är påslagen lyser den; när den är avstängd förblir den mörk.

![Figur 4-1](<../../.gitbook/assets/image (24).png>)

Den inbyggda lysdioden är ansluten till en av RP2040:s allmänna in-/utgångspin, GP25. Detta kan du komma ihåg från tidigare kapitel, är en av de "saknade" GPIO-pinnen som finns på RP2040 men inte brutit ut till en fysisk pin på kanten av din Pico. Även om du inte kan ansluta någon extern hårdvara till pinnen, förutom den inbyggda lysdioden, kan den behandlas precis som alla andra GPIO-pin i dina program-och det är ett bra sätt att lägga till en utgång till dina program utan att behöva några extra komponenter.

Starta **Thony** och, om du inte redan har gjort det, konfigurera den för att ansluta till din Pico som visas i kapitel 2. Klicka **Nytt** och spara programmet som **Blink.py**:

```python
# Importera bibliotek
from machine import Pin
```

Denna korta kodrad är nyckeln till att arbeta med MicroPython på din Pico: den laddar eller importerar en samling av MicroPython-kod som kallas ett bibliotek - i det här fallet **maskinbiblioteket**. **Maskinbiblioteket** innehåller alla instruktioner som MicroPython behöver för att kommunicera med Pico och andra MicroPython-kompatibla enheter, vilket utökar språket för fysisk databehandling. Utan den här raden kommer du inte att kunna styra någon av dina Picos GPIO-pin-och du kommer inte att få den inbyggda LED-lampan att lysa.

{% hint style="info" %}
**SELEKTIVA IMPORT**\
I både MicroPython och Python är det möjligt att importera en del av ett bibliotek, snarare än hela biblioteket. Att göra det kan göra din programmet använder mindre minne och låter dig mixa och matcha funktioner från olika bibliotek. Programmen i den här boken importeras hela biblioteket; annars kan du se program som har rader som från maskinimport Pin ; detta berättar för MicroPython att importera bara "Pin"-funktionen från "maskin"-biblioteket, snarare än hela biblioteket.
{% endhint %}

Maskinbiblioteket öppnar upp det som kallas ett applikationsprogrammeringsgränssnitt (API). Namnet låter komplicerat, men beskriver exakt vad det gör: det ger ett sätt för ditt program, eller applikationen, att kommunicera med Pico via ett gränssnitt. Nästa rad i ditt program ger ett exempel på maskinbibliotekets API:

```python
# Importera bibliotek
from machine import Pin

# Använd GPIO 25 som utgång
led_pico = Pin(25, Pin.OUT)
```

Denna rad definierar ett objekt som heter **led\_pico**, som erbjuder ett vänligt namn som du kan använda för att hänvisa till den inbyggda lysdioden senare i ditt program. Det är tekniskt möjligt att använda valfritt namn här - som susan, gupta eller fish\_sandwich - men det är bäst att hålla sig till namn som beskriver variabelns syfte, för att göra programmet lättare att läsa och förstå.

Den andra delen av raden kallar Pin-funktionen i maskinbiblioteket. Denna funktion, som namnet antyder, är utformad för att hantera din Picos GPIO-pin. För närvarande vet ingen av GPIO-pinnen, inklusive GP25-pinnen som är anslutet till den inbyggda lysdioden vad de ska göra. Det första argumentet, 25, är numret på pinnen du sätter upp; den andra, **Pin.OUT**, säger till Pico att pinnen ska användas som en utgång snarare än en ingång.

Bara den raden räcker för att sätta upp pinnen, men det tänder inte lysdioden. För att göra det måste du berätta för Pico att faktiskt slå på pinnen. Skriv följande kod på nästa rad:

```python
# Importera bibliotek
from machine import Pin

# Använd GPIO 25 som utgång
led_pico = Pin(25, Pin.OUT)

# Tänd LED
led_pico.on()
```

Det kanske inte ser ut något, men den här raden använder också maskinbibliotekets API. Din tidigare rad skapade objektet **led\_pico** som en utgång på pin GP25; denna rad tar objektet och sätter dess värde till 1 för "på" - det kan också ställa in värdet till 0, för "av".

Klicka på **Run**. Du kommer att se lysdioden lysa. Grattis - du har skrivit ditt första fysiska datorprogram!

Du kommer dock att märka att lysdioden förblir tänd: det beror på att ditt program säger till Pico att slå på den, men säger aldrig att den ska stängas av. Du kan lägga till en annan rad längst ner i ditt program:

```python
# Importera bibliotek
from machine import Pin

# Använd GPIO 25 som utgång
led_pico = Pin(25, Pin.OUT)

# Tänd LED
led_pico.on()

# Släck LED
led_pico.off()
```

Klicka på **Run** igen, men lysdioden verkar aldrig tändas. Det beror på att Pico arbetar väldigt, väldigt snabbt - mycket snabbare än du kan se med blotta ögat. Lysdioden tänds, men under så kort tid verkar det vara mörkt. För att åtgärda det måste du sakta ner programmet genom att införa en fördröjning.

Gå tillbaka till toppen av ditt program: klicka i slutet av den första raden och tryck på **ENTER** för att infoga en ny andra rad. Skriv på den här raden:

```python
# Importera bibliotek
from machine import Pin
import utime

# Använd GPIO 25 som utgång
led_pico = Pin(25, Pin.OUT)

# Tänd LED
led_pico.on()

# Släck LED
led_pico.off()
```

Precis som **from machine import Pin** importerar denna rad ett nytt bibliotek till MicroPython: biblioteket '**utime**'. Detta bibliotek hanterar allt som har med tiden att göra, från att mäta det till att infoga förseningar i dina program.

Gå till botten av ditt program och klicka på slutet av raden **led\_pico.on()**, tryck sedan på **ENTER** för att infoga en ny rad:

```python
# Importera bibliotek
from machine import Pin
import utime

# Använd GPIO 25 som utgång
led_pico = Pin(25, Pin.OUT)

# Tänd LED
led_pico.on()
utime.sleep(5)

# Släck LED
led_pico.off()
```

Detta kallar sömnfunktionen från **utime**-biblioteket, vilket gör att ditt program pausas i så många sekunder du skriver - i det här fallet fem sekunder.

Klicka på **Run** igen. Den här gången ser du den inbyggda lysdioden på din Pico tändas, förblir tänd i fem sekunder.

Slutligen är det dags att få lysdioden att blinka. För att göra det måste du skapa en loop. Skriv om ditt program så att det matchar det nedan:

```python
# Importera bibliotek
from machine import Pin
import utime

# Använd GPIO 25 som utgång
led_pico = Pin(25 , Pin.OUT)

# En oändlig loop
while True :
    # Tänd LED
    led_pico.on()
    # Vänta 5 sekunder
    utime.sleep(5)

    # Släck LED
    led_pico.off()
    # Vänta 5 sekunder
    utime.sleep(5)
```

Kom ihåg att raderna inuti loopen måste indenteras med **TAB** så MicroPython vet att de bildar loopen. Klicka på **Run** igen, så ser du LED-strömbrytaren, på i fem sekunder, av i fem sekunder och på igen, ständigt upprepas i en oändlig loop. Lysdioden fortsätter att blinka tills du klickar på **Stop** för att avbryta ditt program och återställa din Pico.

Det finns också ett annat sätt att hantera samma jobb: att använda en växel istället för att ställa in LED:s utgång till 0 eller 1 uttryckligen. Ta bort de fyra sista raderna i ditt program och ersätt dem så att det ser ut så här:

```python
# Importera bibliotek
from machine import Pin
import utime

# Använd GPIO 25 som utgång
led_pico = Pin(25 , Pin.OUT)

# En oändlig loop
while True :
    # Växla mellan på och av
    led_pico.toggle()
    # Vänta 5 sekunder
    utime.sleep(5)
```

Klicka på **Run** igen. Du kommer att se samma aktivitet som tidigare: den inbyggda lysdioden tänd i fem sekunder, slock sedan i fem sekunder och tänd sedan igen i en oändlig loop. Den här gången är ditt program dock två rader kortare: du har optimerat det. Funktionen **toggle()** är tillgängligt på alla digitala utgångspinnen.

{% hint style="success" %}
**Förbättringar: Lysa längre**\
Hur skulle du ändra ditt program för att lysdioden ska lysa för längre?\
Vad sägs om att stanna kvar längre?\
Vad är den minsta försening kan du använda medan du fortfarande ser LED -strömbrytaren på och av?
{% endhint %}

## Med hjälp av ett kopplingsdäck

Projektet i det här kapitlet blir mycket lättare att slutföra om du använder ett kopplingsdäck för att ansluta komponenterna.

![Figur 4-1](<../../.gitbook/assets/image (25).png>)

Kopplingsdäcken är täckta med små hål, för att matcha komponenter, åtskilda 2,54 mm från varandra. Under dessa hål finns metallremsor som fungerar som de bygelkablar du har använt tills nu. Dessa löper i rader tvärs över brädet, med de flesta kopplingsdäck som har ett mellanrum i mitten för att dela dem i två halvor.

Många kopplingsdäck har också bokstäver överst och siffror längs sidorna. Dessa låter dig hitta ett särskilt hål: A1 är det övre vänstra hörnet, B1 är hålet till höger omedelbart, medan B2 är ett hål därifrån. A1 är ansluten till B1 med de dolda metallremsorna, men inget hål som är markerat med ett 1 är någonsin anslutet till något hål som är markerat med ett 2 om du inte själv lägger till en bygelkabel.

Större kopplingsdäck har också hålremsor längs sidorna, typiskt märkta med röda och svarta eller röda och blå ränder. Dessa är strömskenorna och är utformade för att göra ledningen enklare: du kan ansluta en enda tråd från en av dina Picos jordpin till en av kraftskenorna - vanligtvis markerade med en blå eller svart rand och en minussymbol - för att ge en gemensam grund för många komponenter på kopplingsdäcken, och du kan göra detsamma om din krets behöver 3,3 V eller 5 V ström. Obs: Alla hål som är förbundna med en rand är anslutna; ett gap indikerar ett avbrott.

Att lägga till elektroniska komponenter till ett kopplingsdäck är enkelt: lägg bara in sina ledningar(de utstickande metalldelarna) med hålen och tryck försiktigt tills komponenten är på plats. För anslutningar du behöver göra utöver dem som kopplingsdäcken gör för dig kan du använda hane-hane (M2M) bygelkablar; för anslutningar från kopplingsdäcken till externa enheter, som din Raspberry Pi Pico, använd hona-hona (M2F) bygelkablar.

Försök aldrig att klämma in mer än en komponentledning eller bygelkabel i ett enda hål på brödbrädet. Kom ihåg: hål är anslutna i rader, förutom delningen i mitten, så en komponentledning i A1 är elektriskt ansluten till allt du lägger till i B1, C1, D1 och E1.

Skjut in din Pico i kopplingsdäcken så att den går över mittgapet och mikro-USB porten ligger högst upp på brädet(se Figur 4-3). Den övre vänstra pinnen, Pin 0, ska finnas i raden på kopplingsdäcken som är markerad med en 1, om din kopplingsdäck är numrerad. Innan du trycker ner din Pico, se till att huvudpinnen är korrekt placerade - om du böjer en pin kan det vara svårt att räta ut den igen utan att den går sönder.

![Figur 4-2](<../../.gitbook/assets/image (26).png>)

Tryck försiktigt ner Pico tills plastdelarna på huvudpinnen vidrör kopplingsdäcken. Det betyder att metalldelarna på huvudpinnen är helt isatta och ger god elektrisk kontakt med kopplingsdäcken.

## Nästa steg: en extern lysdiod

Hittills har du arbetat med din Pico och slagit på och av den inbyggda lysdioden. Mikrokontrollers används dock vanligtvis med externa komponenter - och din Pico är inget undantag.

För detta projekt behöver du:

* Ett kopplingsdäck
* Hane-hane (M2M) bygelkablar
* En LED
* Ett 330Ω motstånd-eller så nära 330Ω som du har tillgängligt.

Om du inte har ett kopplingsdäck kan du använda hona-till-hona (F2F) bygelkablar, men kretsen blir bräcklig och lätt att bryta.

{% hint style="info" %}
Motståndet är en viktig komponent i denna krets: det skyddar både din Pico och lysdioden genom att begränsa mängden elektrisk ström som LED kan dra. Utan det kan lysdioden dra för mycket ström och bränna sig - eller din Pico - ut. När det används så här är motståndet känt som en strömbegränsande motstånd . Det exakta värdet på motståndet du behöver beror på vilken LED du är använder, men 330Ω fungerar för de vanligaste lysdioderna. Ju högre värde, desto dämpa LED; ju lägre värde desto ljusare lysdiod.\
Anslut aldrig en lysdiod till din Pico utan ett strömbegränsande motstånd, såvida du inte vet att lysdioden har ett inbyggt motstånd med lämpligt värde.
{% endhint %}

Håll lysdioden i fingrarna: du ser att en av dess ledningar är längre än den andra. Den längre ledningen är känd som anoden och representerar den positiva sidan av kretsen; den kortare ledningen är katoden och representerar den negativa sidan. Anoden måste anslutas till en av dina Picos GPIO-pin via motståndet; katoden måste anslutas till en jordpin.

Börja med att ansluta motståndet: ta ena änden(det spelar ingen roll vilken) och sätt in den i kopplingsdäcken på samma rad som din Picos GP15-pin längst ned till vänster-om du använder ett numrerat kopplingsdäck med din Pico isatt högst upp bör detta vara rad 20. Tryck in den andra änden i en ledig rad längre ner på kopplingsdäcken - vi använder rad 24.

Ta lysdioden och tryck in det längre benet - anoden - i samma rad som motståndets ände. Skjut in det kortare benet - katoden - i samma rad men tvärs över mittgapet i kopplingsdäcken, så är det uppradat men inte elektriskt anslutet till det längre benet utom genom själva lysdioden. Slutligen sätter du in en hane-hane (M2M) bygelkabel i samma rad som det kortare benet på lysdioden, ansluter den antingen direkt till en av dina Picos jordpin (via ett annat hål i raden) eller till den negativa sidan av kopplingsdäckens strömskena. Om du ansluter den till kraftskenan, avsluta kretsen med att ansluta skenan till en av dina Picos jodpin. Din färdiga krets ska se ut som figur 4-3.

![Figur 4-3](<../../.gitbook/assets/image (27).png>)

Att styra en extern lysdiod i MicroPython är inte annorlunda än att styra din Picos interna lysdiod: bara pinnumret ändras. Om du stängde **Thony**, öppna det igen och ladda ditt **Blink.py** program från tidigare i kapitlet. Hitta raden:

```python
led_pico = Pin(25, Pin.OUT)
```

Redigera pinnumret och ändra det från 25 - pinnen som är anslutet till din Picos interna lysdiod - till 15, pinnen som du anslöt den externa lysdioden till. Redigera också namnet du skapade: du använder inte den inbyggda lysdioden längre, så låt det säga **led\_extern** istället. Du måste också ändra namnet någon annanstans i programmet tills det ser ut så här:

```python
# Importera bibliotek
from machine import Pin
import utime

# Använd GP15 för att styra en extern lysdiod
led_extern = Pin(15, Pin.OUT)

# En oändlig loop
while True :
    # Växla mellan att slå på och slå av lysdioden
    led_extern.toggle()
    # Vänta 5 sekunder
    utime.sleep(5)
```

{% hint style="info" %}
**NAMNKONVENTIONER**![](https://translate.googleusercontent.com/image\_60.png)\
Du behöver inte verkligen behöver för att ändra namnet på programmet: det skulle köra precis samma om du hade lämnat det på **led\_pico** , som det är bara pin -numret som verkligen spelar roll. När du kommer tillbaka till programmet senare, men det skulle vara mycket förvirrande att ha ett objekt som heter **led\_pico** som tänder en extern lysdiod - så försök att vänja dig vid att se till att dina namn stämmer med deras syfte!
{% endhint %}

{% hint style="success" %}
**Förbättringar: Fler lysdioder**\
Kan du ändra programmet för att lysa upp både ombord och externa lysdioder samtidigt? Kan du skriva ett program som lyser den inbyggda lysdioden när den externa lysdioden är omkopplad av, och tvärtom? Kan du förlänga kretsen till att inkludera mer än en extern LED? Kom ihåg att du behöver en strömbegränsningsmotstånd för varje LED du använder!
{% endhint %}

## Ingångar: läsa av en knapp

Utgångar som lysdioder är en sak, men "ingång/utgång" -delen av "GPIO" innebär att du också kan använda pin som ingångar. För detta projekt behöver du ett kopplingsdäck, man-till-man-bygelkablar och en tryckknappsbrytare. Om du inte har ett kopplingsdäck kan du använda hona-hona (F2F) bygelkablar, men knappen blir mycket svårare att trycka på utan att oavsiktligt bryta kretsen.

Ta bort alla andra komponenter från din kopplingsdäck utom din Pico, och börja med att lägga till tryckknappen. Om din tryckknapp bara har två ben, se till att de är i olika numrerade rader på kopplingsdäcken någonstans nedanför din Pico. Om den har fyra ben, vrid den så att sidorna benen kommer ifrån ligger längs kopplingsdäckens rader och de platta benfria sidorna är upptill och nedtill innan du trycker hem den så att den går över mittskivan på kopplingsdäcken.

Anslut den positiva kraftskenan på din kopplingsdäck till din Picos 3V3-pin, och därifrån till ett av brytarens ben; Anslut sedan det andra benet till pin GP14 på din Pico - det är det precis ovanför pinnen du använde för LED-projektet, och ska vara i rad 19 på din kopplingsdäck.

Om du använder tryckknapp med fyra ben fungerar din krets bara om du använder rätt benpar: benen är parvis sammankopplade, så du måste antingen använda de två benen på samma sida eller (figur 4-4) diagonalt motsatta ben.

![Figur 4-4](<../../.gitbook/assets/image (28).png>)

Starta **Thony**, om du inte redan har gjort, och starta ett nytt program med den vanliga raden:

```python
# Importera bibliotek
from machine import Pin
```

Därefter måste du använda maskinens API för att ställa in en pin som en ingång, snarare än en utgång:

```python
# Använd GP14 för att läsa av en tryckknapp
knapp = Pin(14, Pin.IN, Pin.PULL_DOWN)
```

Detta fungerar på samma sätt som dina LED-projekt: ett objekt som kallas 'knapp' skapas, vilket inkluderar pinnumret-GP14, i det här fallet-och konfigurerar det som en ingång med motståndet inställt på nedrullning. Att skapa objektet betyder dock inte att det kommer att göra någonting av sig själv - precis som att skapa LED -objekten tidigare inte fick lysdioderna att lysa.

För att faktiskt läsa knappen måste du använda maskinens API igen - den här gången använder du värdefunktionen för att läsa, snarare än att ställa in, pinnets värde. Skriv följande rad:

```python
# Skriv ut värdet på knappen
print(knapp.value())
```

Klicka på **Run** och spara ditt program som knapp.py - kom ihåg att se till att det sparar på Raspberry Pi Pico. Ditt program kommer att skriva ut ett enda nummer: värdet på ingången på GP14.

Eftersom ingången använder ett neddragningsmotstånd blir detta värde 0, så att du vet att knappen inte trycks in.

Håll ned knappen med fingret och tryck på **Run** igen. Den här gången ser du värdet 1 tryckt på konsolen: genom att trycka på knappen har kretsen slutförts och värdet som har lästs från pinnen ändrats.

För att läsa knappen kontinuerligt måste du lägga till en loop i ditt program. Redigera programmet så att det lyder enligt nedan:

```python
# Importera bibliotek
from machine import Pin, utime

# Använd GP14 för att läsa av en tryckknapp
knapp = Pin(14, Pin.IN, Pin.PULL_DOWN)

# En oändlig loop
while True :
    # Om knappen är nedtryckt
    if knapp.value() == 1 :
        # Skriv ut värdet på knappen
        print("Du tryckte på knappen!")
        # Vänta 2 sekunder
        utime.sleep(2)
```

Klicka på **Run** igen. Den här gången händer ingenting förrän du trycker på knappen; när du gör det ser du ett meddelande som skrivs ut till konsolen. Förseningen är emellertid viktig: kom ihåg, din Pico går mycket snabbare än du kan läsa, och utan fördröjning kan ett kort tryck på knappen skriva ut hundratals meddelanden till konsolen!

Du ser meddelandet skrivas ut varje gång du trycker på knappen. Om du håller knappen intryckt längre än fördröjningen på två sekunder, skrivs meddelandet ut varannan sekund tills du släpper knappen.

## Inputs och outputs: att sätta ihop allt

De flesta kretsar har mer än en komponent, varför din Pico har så många GPIO-pin. Det är dags att sätta ihop allt du har lärt dig för att bygga en mer komplex krets: en enhet som slår på och av en LED med en knapp.

![Figur 4-5](<../../.gitbook/assets/image (29).png>)

I själva verket kombinerar denna krets båda de tidigare kretsarna till en. Du kanske kommer ihåg att du använde pin GP15 för att driva den externa lysdioden och pin GP14 för att läsa knappen; bygg om nu din krets så att både lysdioden och knappen är på kopplingsdäcken samtidigt, fortfarande anslutna till GP15 och GP14 (figur 4-5). Glöm inte det strömbegränsande motståndet för lysdioden!

Starta ett nytt program i **Thony** och spara programmet som **Switch.py**. Börja importera de två bibliotek som ditt program behöver:

```python
# Importera bibliotek
from machine import Pin, utime
```

Ställ sedan in både in- och utgångspinnen:

```python
# Använd GP15 för att styra en lysdiod
led_extern = Pin(15, Pin.OUT)
# Använd GP14 för att läsa av en tryckknapp
button = Pin(14, Pin.IN, Pin.PULL_DOWN)
```

Skapa sedan en loop som läser av knappen:

```python
# En oändlig loop
while True:
    # Om knappen är nedtryckt
    if knapp.value() == 1:
```

Istället för att skriva ut ett meddelande till konsolen, men den här gången kommer du att växla mellan utgångspinnen och lysdioden som är ansluten till den baserat på värdet på ingångspinnen. Skriv följande, kom ihåg att det kommer att behöva indragas med åtta mellanslag - som **Thony** borde ha hanterat automatiskt när du tryckte på **ENTER** i slutet av raden ovan:

```python
# Sätt utgångspinnen till 1 (tänd)
led_extern.on()
# Vänta 2 sekunder
utime.sleep(2)
```

Det räcker för att tända lysdioden, men du måste också stänga av den igen när knappen inte trycks in. Lägg till följande nya rad med **BACKSPACE**-tangenten för att radera fyra av de åtta mellanslag - vilket betyder att raden inte kommer att vara en del av if-satsen, utan kommer att utgöra en del av den oändliga loopen:

```python
# Sätt utgångspinnen till 0 (släckt)
led_extern.off()
```

Ditt färdiga program ska se ut så här:

```python
# Importera bibliotek
from machine import Pin, utime

# Använd GP15 för att styra en lysdiod
led_extern = Pin(15 , Pin.OUT)
# Använd GP14 för att läsa av en tryckknapp
knapp = Pin(14 , Pin.IN, Pin.PULL_DOWN)

# En oändlig loop
while True :
    # Om knappen är nedtryckt
    if knapp.value() == 1 :
        # Sätt utgångspinnen till 1 (tänd)
        led_extern.on()
        # Vänta 2 sekunder
        utime.sleep(2)
    # Sätt utgångspinnen till 0 (släckt)
    led_extern.off()
```

Klicka på **Run**. Till en början kommer ingenting att hända; tryck på knappen, så ser du lysdioden tändas. Släpp knappen; efter två sekunder slocknar lysdioden igen tills du trycker på knappen igen.

Grattis: du har byggt din första krets som styr en pin baserat på ingången från en annan - en byggsten för större saker!

{% hint style="success" %}
**Förbättringar: ATT BYGGA UPP**\
Kan du ändra ditt program så att det både tänder lysdioden och skriver ut ett statusmeddelande till konsolen?\
Vad skulle du behöva ändra så att lysdioden lyser när knappen inte trycks in och stänga av när det är?\
Kan du lägga till fler knappar och lysdioder?
{% endhint %}
