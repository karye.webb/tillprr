# Prova en summer

I denna uppgift ska vi koppla in en summer till Raspberry Pi Pico och få den att låta i olika toner.

![alt text](../../.gitbook/assets/image-6.png)

## Material

För att genomföra denna uppgift behöver du följande material:

* 1 x Raspberry Pi Pico
* 1 x Summer
* 3 x ledningar

## Anslutningsschema

Anslut summer till Raspberry Pi Pico enligt anslutningsschemat ovan.

## Spela en ton

För att spela en ton med en summer behöver vi generera en PWM-signal med en viss frekvens. Summerns tonhöjd bestäms av frekvensen på PWM-signalen. För att generera en PWM-signal med en viss frekvens kan vi använda följande kod:

```python
from machine import Pin, PWM
import time

summer = PWM(Pin(15))    # Anslut summer till pinne 15

frekvens = 1000           # Tonfrekvens i Hz
summer.freq(frekvens)     # Sätt tonfrekvens

summer.duty_u16(32768)    # Sätt ljusstyrka
time.sleep(1)             # Spela ton i 1 sekund

summer.duty_u16(0)        # Stäng av ton
```

### Uppgifter

* Skapa larmsignal upprepade gånger kort-paus-lång-paus
* Skapa morsesignal SOS dvs 3 korta signaler, 3 långa signaler, 3 korta signaler

## Spela olika toner

För att spela toner med en summer behöver vi använda en PWM (Pulse Width Modulation) signal. PWM är en teknik som används för att generera analoga signaler med en digital signal. Raspberry Pi Pico har flera PWM-kanaler som kan användas för att generera PWM-signaler.

För att generera en PWM-signal för att spela en ton med en summer kan vi använda följande kod:

```python
from machine import Pin, PWM
import time

summer = PWM(Pin(15))    # Anslut summer till pinne 15

# Tonfrekvenser
toner = {
    'C4': 262,
    'D4': 294,
    'E4': 330,
    'F4': 349,
    'G4': 392,
    'A4': 440,
    'B4': 494,
    'C5': 523
}

# Spela toner
for ton, frekvens in toner.items():
    summer.freq(frekvens)    # Sätt tonfrekvens
    summer.duty_u16(32768)   # Sätt ljusstyrka
    time.sleep(0.5)          # Spela ton i 0.5 sekunder
    summer.duty_u16(0)       # Stäng av ton
    time.sleep(0.1)          # Vänta 0.1 sekunder
```

I exemplet ovan spelar vi tonerna C4, D4, E4, F4, G4, A4, B4 och C5 med en summer ansluten till pinne 15 på Raspberry Pi Pico. Vi använder en PWM-signal för att generera tonerna och sätter tonfrekvensen och ljusstyrkan för varje ton.

Prova att ändra tonerna och frekvenserna i koden för att spela olika melodier med din summer.

