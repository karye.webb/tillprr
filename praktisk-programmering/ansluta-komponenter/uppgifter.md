# Uppgifter

## Blinkande LED

```python
# Importera bibliotek
from machine import Pin, utime

# Anslut GP25 till LED
led = Pin(25, Pin.OUT)

# En oändlig loop
while True:
    # Sätt led till 3.3V
    led.on()
    # Vänta 1 sekund
    utime.sleep(1)
    # Sätt led till 0V
    led.off()
    # Vänta 1 sekund
    utime.sleep(1)
```

### Uppgift: blinka grön led

* Lägg till en grön led som blinkar i otakt

```python
# Importera bibliotek
from machine import Pin, utime

# Anslut GP25 till LED
led = Pin(25, Pin.OUT)

# Blinka röd led
while True:
    led.on()
    utime.sleep(1)
    led.off()
    utime.sleep(1)
    
# Uppgift: blinka grön led
```

## Styra ljustyrka på LED

```python
# 
from machine import Pin, PWM
import utime

# Anslut GP25 till LED
led = Pin(25)
# Använd PWM för att styra ljustyrka
pwm = PWM(led)
# Sätt frekvens till 100Hz
pwm.freq(100)

# En loop
duty = 0
direction = 1
for repeat in range(99999999):
    # Öka ljustyrkan
    duty += direction
    # Om ljustyrkan är för hög eller för låg, ändra riktning
    if duty > 255:
        duty = 255
        direction = -1
    elif duty < 0:
        duty = 0
        direction = 1
    
    # Sätt ljustyrkan
    pwm.duty_u16(duty * duty)
    # Vänta 0.004 sekunder
    utime.sleep(0.004)
```

## Styra med knapp

```python
# Importera bibliotek
from machine import Pin, utime

# Använd GP25 för LED
led = Pin(15, Pin.OUT)

# Använd GP13 för knapp
knapp = Pin(13, Pin.IN, Pin.PULL_DOWN)

# Tänd led
led.on()
# Skriv ut knappens värde
print(knapp.value())

# Använd knappen för att öka ljusstyrkan på led
```
