# Prova en potentiometer

I denna övning kommer vi att ansluta en potentiometer till en Raspberry Pi Pico och skriva ett enkelt program för att läsa av värdet från potentiometern.

## Material

För att genomföra denna övning behöver du följande material:

* 1 x Raspberry Pi Pico
* 1 x Potentiometer 10 kΩ
* 3 x ledningar

## Anslutningsschema

Anslut potentiometern till Raspberry Pi Pico enligt anslutningsschemat nedan:

![alt text](../../.gitbook/assets/image-5.png)

## Läsa av potentiometern

För att läsa av potentiometern behöver vi använda en analog-digital-omvandlare (ADC). Raspberry Pi Pico har en inbyggd ADC som kan användas för att läsa av analoga signaler. I detta fall kommer vi att använda en potentiometer som en spänningsdelare och läsa av spänningen på potentiometerns mittledare.

```python
from machine import ADC, Pin
import time

potentiometer = ADC(Pin(26))    # Anslut potentiometern till pinne 26
reading = adc.read_u16()        # Läs av potentiometern
print(reading)                  # Skriv ut värdet
```
## Kontinuerlig avläsning

För att kontinuerligt läsa av potentiometern kan vi använda en while-loop:

```python
from machine import ADC, Pin
import time

potentiometer = ADC(Pin(26))    # Anslut potentiometern till pinne 26

while True:
    reading = adc.read_u16()    # Läs av potentiometern
    print(reading)              # Skriv ut värdet
    time.sleep(0.1)             # Vänta 0.1 sekunder
```

## Styra en LED med potentiometern

![alt text](../../.gitbook/assets/image-8.png)

Nu när vi kan läsa av potentiometern kan vi använda dess värde för att styra en LED. Vi kan koppla en LED till en annan pinne på Raspberry Pi Pico och använda potentiometerns värde för att styra ljusstyrkan på LED.

Koppla en LED till pinne 15 på Raspberry Pi Pico och anslut en motstånd på 220 Ω mellan LED och jord.

Vårt program kommer att läsa av potentiometern och använda dess värde för att styra ljusstyrkan på LED:

```python
from machine import ADC, Pin, PWM
import time

potentiometer = ADC(Pin(26))    # Anslut potentiometern till pinne 26
led = PWM(Pin(15))              # Anslut LED till pinne 15

while True:
    reading = adc.read_u16()    # Läs av potentiometern
    brightness = reading / 65535 # Beräkna ljusstyrkan
    led.duty_u16(int(brightness * 65535)) # Sätt ljusstyrkan på LED
    time.sleep(0.1)             # Vänta 0.1 sekunder
```

## Uppgift

![alt text](../../.gitbook/assets/image-7.png)

* Koppla in en summer till Raspberry Pi Pico och använd potentiometern för att styra tonhöjden på summer.