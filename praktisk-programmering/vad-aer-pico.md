# Introduktion till Raspberry Pi Pico?

## Vad är Pico?

Raspberry Pi Pico är en mikrokontroller som är utvecklad av Raspberry Pi Foundation. Pico är en mikrokontroller som är baserad på RP2040-chipet som är utvecklat av Raspberry Pi Foundation. RP2040-chipet är en dubbelkärnig ARM Cortex-M0+ mikrokontroller som är klockad på 133 MHz. Pico har 264 KB RAM och 2 MB flashminne. Pico har 26 GPIO-pinnar som kan användas för att koppla upp mot olika sensorer och enheter. Pico har även stöd för SPI, I2C, UART och PWM.

{% embed url="https://youtu.be/zlKJ5hvfs6s" %}

## Pico projekt

Här är några exempel på projekt som du kan göra med Raspberry Pi Pico:

{% embed url="https://youtu.be/PF3ho7OrgcY?si=wMRiW7yW9TvdX2_r" %}

## Kursbok till Pico

![](<../.gitbook/assets/image (11).png>)

{% file src="../.gitbook/assets/RPi_PiPico_Digital_v10.pdf" %}
Get started with MicroPython on Raspberry Pico
{% endfile %}

![](<../.gitbook/assets/image (3).png>)

{% file src="../.gitbook/assets/210226 Get Started with Micropython Errata.pdf" %}
Errata - några fel i boken
{% endfile %}

## Bra länkar

* [https://www.raspberrypi.org/products/raspberry-pi-pico/](https://www.raspberrypi.org/products/raspberry-pi-pico/)
* [General information about MicroPython on RP2xxx](https://docs.micropython.org/en/latest/rp2/general.html)