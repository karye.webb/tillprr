---
description:
  Bygg ett enkelt reaktionstimingspel med en LED och tryckknappar för en eller
  två spelare
---

# Labb 3 - reaktionsspel

I det här kapitlet kommer du att bygga ett enkelt reaktionstimingspel, se vem av dina vänner som kommer att vara först med att trycka på en knapp när en lampa slocknar.

Studiet av reaktionstid är känt som mental kronometri och även om det bildar en hård vetenskap, är den också grunden för massor av färdighetsbaserade spel-inklusive det du ska bygga. Din reaktionstid - den tid det tar din hjärna att bearbeta behovet av att göra något och skicka signaler för att något ska hända - mäts i millisekunder: den genomsnittliga mänskliga reaktionstiden är cirka 200–250 millisekunder, även om vissa människor njuter av betydligt snabbare reaktionstider som ger dem en riktig fördel i spelet!

För detta projekt behöver du din Pico; en kopplingsdäck; en LED i valfri färg; ett enda 330Ω motstånd; två tryckknappar; och ett urval av hane-hane (M2M) bygelkablar. Du behöver också en mikro-USB kabel och för att ansluta din Pico till din Raspberry Pi eller annan dator som kör **Thony**.

## Ett spel för en spelare

Börja med att placera din LED i din kopplingsdäck så att den ligger över mittskiktet. Kom ihåg att lysdioder bara fungerar när de är på rätt väg: se till att du vet vilket som är det längre benet, eller anoden, och vilket som är det kortare benet, katoden.

Använd ett 330Ω strömbegränsande motstånd, för att skydda både lysdioden och din Pico, led det längre benet på lysdioden till pin GP15 längst ned till vänster på din Pico sett uppifrån med mikro-USB kabeln upptill. Om du använder en numrerad kopplingsdäck och har din Pico införd längst upp, kommer detta att vara kopplingsdäck rad 20.

Ta en bygelkabel och anslut det kortare benet på lysdioden till kopplingsdäckns jordskena. Ta en till och anslut jordskenan till en av dina Picos markpinnar (GND) i figur 6-1 har vi använt markpinnen på rad tre på brödbrädet.

![Figur 6-1](<../.gitbook/assets/image (32).png>)

Lägg sedan till tryckknappen enligt figur 6-1 . Ta en bygelkabel och anslut en av tryckknappens omkopplare till pin GP14, precis bredvid pinnen du använde för din LED. Använd en annan bygelkabel för att ansluta det andra benet-det diagonalt mittemot det första, om du använder en fyrbent tryckknappsbrytare-till kopplingsdäckns kraftskena. Slutligen, ta en sista bygelkabel och anslut strömskenan till din Picos 3V3-pin.

Din krets har nu allt den behöver för att fungera som ett enkelt spel för en spelare: lysdioden är utmatningsenheten, som tar platsen för TV:n du normalt skulle använda med en spelkonsol; tryckknappsbrytaren är styrenheten; och din Pico är spelkonsolen, om än en betydligt mindre än du brukar se!

Nu måste du faktiskt skriva spelet. Som alltid, anslut din Pico och starta **Thony**. Skapa ett nytt program och spara programmet som **Reaction\_Game.py. **Börja med att importera maskinbiblioteket så att du kan styra din Picos GPIO-pin:

```python
from machine import Pin
```

Du kommer också att behöva **utime**-biblioteket:

```python
import utime
```

Dessutom behöver du ett nytt bibliotek: **urandom**, som hanterar att skapa slumpmässiga nummer - en viktig del i att göra ett spel roligt, och som används i det här spelet för att förhindra att en spelare som har spelat det förut helt enkelt räknar ner ett fast antal sekunder från att klicka på knappen **Run**.

Ställ sedan in en pressad variabel till **False** (mer om detta senare) och ställ in de två pinnen du använder: GP15 för LED och GP14 för tryckknappen .

```python
tryckt = False
led = Pin(15, Pin.OUT)
knapp = Pin(14, Pin.IN, Pin.PULL_DOWN)
```

I tidigare kapitel har du hanterat knappar i antingen huvudprogrammet eller i en separat tråd. Den här gången kommer du dock att ta ett annat och mer flexibelt tillvägagångssätt: avbryt förfrågningar eller IRQ. Namnet låter komplext, men det är verkligen enkelt: tänk dig att du läser en bok, sida för sida, och någon kommer fram till dig och ställer en fråga. Den personen utför en avbrottsbegäran: ber dig sluta med det du gör, svara på deras fråga och låter dig sedan gå tillbaka till att läsa din bok.

En MicroPython-avbrottsbegäran fungerar på exakt samma sätt: det tillåter något, i detta fall trycket på en tryckknappsbrytare, att avbryta huvudprogrammet. På vissa sätt liknar det en tråd, eftersom det finns en bit kod som sitter utanför huvudprogrammet. Till skillnad från en tråd,

Koden körs dock inte hela tiden: den körs bara när avbrottet utlöses. Börja med att definiera en hanterare för avbrottet. Denna, känd som en återuppringningsfunktion, är koden som körs när avbrottet utlöses. Som med alla andra kapslade koder måste hanterarens kod - allt efter den första raden - indragas med fyra mellanslag för varje nivå; **Thony** kommer att göra detta åt dig automatiskt.

```python
def button_handler(pin):
    globalt tryckt
    if not tryckt:
        tryckt = True
        print(pin)
```

Den här hanteraren börjar med att kontrollera statusen för den variabeln tryckt och sedan ställa in den på **True **för att ignorera ytterligare knapptryckningar (därmed avslutas spelet). Den skriver sedan ut information om pinnen som är ansvarigt för att utlösa avbrottet. Det är inte så viktigt just nu - du har bara en pin konfigurerad som en ingång, GP14, så avbrottet kommer alltid från det pinnen - men låter dig enkelt testa ditt avbrott.

Fortsätt ditt program nedan, kom ihåg att radera indrag som Thony har skapat automatiskt - följande kod är inte en del av hanteraren:

```python
led.on()
utime.sleep(urandom.uniform(5, 10))
led.off()
```

Den här koden är omedelbart bekant för dig: den första raden tänder lysdioden, ansluten till pin GP15; nästa rad pausar programmet; sista raden släcker lysdioden igen - spelarens signal att trycka på knappen. I stället för att använda en fast fördröjning använder den sig dock av **urandom**-biblioteket för att pausa programmet i mellan fem och tio sekunder - den "enhetliga" delen avser en uniform fördelningen mellan dessa två nummer.

För närvarande är det dock inget som tittar på knappen som trycks. Du måste ställa in avbrottet för det genom att skriva följande rad längst ner i ditt program:

```python
button.irq(trigger = Pin.IRQ_RISING, handler = button_handler)
```

Att ställa in ett avbrott kräver två saker: en utlösare och en hanterare. Utlösaren berättar för din Pico vad den ska leta efter som en giltig signal för att avbryta vad den gör. hanteraren, som du definierade tidigare i ditt program, är koden som körs efter att avbrottet har utlösts. I det här programmet är din trigger **IRQ\_RISING**: det betyder att avbrottet utlöses när pinnens värdet stiger från lågt, dess standardläge tack vare det inbyggda neddragningsmotståndet, till högt, när knappen som är ansluten till 3V3 trycks in. En utlösare av **IRQ\_FALLING **skulle göra det motsatta: utlösa avbrottet när pinnen går från högt till lågt. För din krets utlöses **IRQ\_RISING **så snart knappen trycks in; **IRQ\_FALLING **utlöses bara när knappen släpps.

{% hint style="info" %}
**STIGNING OCH FALL AV IRQS**\
Om du behöver skriva ett program som utlöser ett avbrott när en nål ändras, utan att bry sig om den stiger eller fallande kan du kombinera de två utlösarna med hjälp av ett rör eller vertikal stapelsymbol(|):\
**button.irq(trigger = Pin.IRQ\_RISING | Pin.IRQ\_FALLING, handler = button\_handler)**
{% endhint %}

Ditt program ska se ut så här:

```python
from machine import Pin, utime
import urandom

tryckt = False
led = Pin(15, Pin.OUT)
knapp = Pin(14, Pin.IN, Pin.PULL_DOWN)

def button_handler(pin):
    globalt tryckt
    if not tryckt:
        tryckt = True
        print(pin)

led.on()
utime.sleep(urandom.uniform(5, 10))
led.off()
button.irq(trigger = Pin.IRQ_RISING, handler = button_handler)
```

Klicka på knappen **Run**. Du kommer att se lysdioden lysa: det är din signal för att göra dig redo med fingret på knappen. När lysdioden släcks trycker du på knappen så snabbt du kan.

När du trycker på knappen utlöser den hanterarkoden du skrev tidigare. Titta i konsolen: du kommer att se att din Pico har skrivit ut ett meddelande som bekräftar att avbrottet utlöstes av en pin GP14. Du kommer också att se en annan detalj: mode = IN berättar att pinnen har konfigurerats som en ingång. Det meddelandet gör dock inte så mycket spel: för det behöver du ett sätt att ta tid på spelarens reaktionshastighet. Börja med att radera radutskriften (pin) från din knapphanterare - du behöver det inte längre.

Gå till botten av ditt program och lägg till en ny rad, precis ovanför där du ställde in avbrottet:

```python
timer_start = utime.ticks_ms()
```

Detta skapar en ny variabel som kallas **timer\_start **och fyller den med utdata från funktionen **utime.ticks\_ms()**, som räknar antalet millisekunder som har gått sedan **utime**-biblioteket började räkna. Detta ger en referenspunkt: tiden strax efter att lysdioden slocknade och strax innan avbrottsutlösaren blev redo att läsa knapptryckningen. Gå sedan tillbaka till din knapphanterare och lägg till följande två rader, kom ihåg att de måste indragas med fyra mellanslag så att MicroPython vet att de utgör en del av den kapslade koden:

```python
timer_reaction = utime.ticks_diff(utime.ticks_ms(), timer_start)
print("Din reaktionstid var " + str(timer_reaction) + " millisekunder!")
```

Den första raden skapar en annan variabel, den här gången när avbrottet faktiskt utlöstes - med andra ord när du tryckte på knappen. Istället för att bara ta en avläsning från **utime.ticks\_ms()** som tidigare använder den dock **utime.ticks\_diff()** - en funktion som ger skillnaden mellan när denna kodrad utlöses och referenspunkten i variabeln **timer\_start **.

Den andra raden skriver ut resultatet, men använder sammanfogning för att formatera det snyggt. Den första texten, eller strängen, berättar för användaren vad numret som följer betyder; de + innebär att allt som kommer nästa ska skrivas tillsammans med den strängen. I det här fallet är nästa innehåll innehållet i variabeln **timer\_reaction **- skillnaden, i millisekunder, mellan när du tog referenspunkten för timern och när knappen trycktes och avbrottet utlöstes.

Slutligen sammanfogar den sista raden ytterligare en sträng så att användaren vet att antalet mäts i millisekunder, och inte någon annan enhet som sekunder eller mikrosekunder. Var uppmärksam på avstånd: du ser att det finns ett efterföljande mellanslag efter 'var' och före slutcitatet för den första strängen, och ett ledande mellanslag efter det öppna citatet för den andra strängen och före ordet 'millisekunder'. Utan dessa kommer den sammanfogade strängen att skriva ut ungefär "Din reaktionstid var 323 millisekunder".

Ditt program ska nu se ut så här:

```python
from machine import Pin, utime
import urandom

tryckt = False
led = Pin(15, Pin.OUT)
knapp = Pin(14, Pin.IN, Pin.PULL_DOWN)

def button_handler(pin):
    globalt tryckt
    if not tryckt:
        tryckt = True
        timer_reaction = utime.ticks_diff(utime.ticks_ms(), timer_start)  print("Din reaktionstid var " + str(timer_reaction) + "millisekunder!")

led.on()
utime.sleep(urandom.uniform(5, 10))
led.off()
timer_start = utime.ticks_ms()
button.irq(trigger = Pin.IRQ_RISING, handler = button_handler)
```

{% hint style="success" %}
**UTMANING: ANPASSNING**\
Kan du justera ditt spel så att lysdioden lyser under en längre tid?\
Vad sägs om att vara upplyst en kortare tid?\
Kan du anpassa meddelande som skrivs ut till konsolen och lägg till ett andra meddelande gratulerar spelaren?
{% endhint %}

Klicka på knappen **Run** igen, vänta tills lysdioden slocknar och tryck på knappen. Den här gången, istället för en rapport om pinnen som utlöste avbrottet, ser du en rad som berättar hur snabbt du tryckte på knappen - en mätning av din reaktionstid. \
Klicka på knappen **Run** igen och se om du kan trycka på knappen snabbare den här gången - i det här spelet försöker du få en så låg poäng som möjligt!

Spel för en spelare är roligt, men det är ännu bättre att engagera dina vänner. Du kan börja med att bjuda in dem att spela ditt spel och jämföra dina höga - eller rättare sagt låga - poäng för att se vem som har snabbast reaktionstid. Därefter kan du modifiera ditt spel så att du kan gå head-to-head!

Börja med att lägga till en andra knapp i din krets. Koppla upp det på samma sätt som den första knappen, med ett ben som går till strömskenan på din kopplingsdäck men med det andra som pinar GP16 - pinnen över brädet från GP14 där lysdioden är ansluten, i det motsatta hörnet av din Pico . Se till att de två knapparna är tillräckligt långt ifrån varandra så att varje spelare har plats att sätta fingret på sin knapp. Din färdiga krets bör se ut som figur 6-2.

![Figur 6-2](<../.gitbook/assets/image (33).png>)

Även om din andra knapp nu är ansluten till din Pico, vet den inte vad han ska göra med den ännu. Gå tillbaka till ditt program i Thony och hitta var du ställde in den första knappen. Lägg direkt till under den här raden:

```python
right_button = Pin(16, Pin.IN, Pin.PULL_DOWN)
```

Du kommer att märka att namnet nu anger vilken knapp du arbetar med: den högra knappen på panelen. För att undvika förvirring, redigera raden ovan så att du gör det klart vad som var den enda knappen på tavlan som nu är vänster knapp:

```python
left_button = Pin(14, Pin.IN, Pin.PULL_DOWN)
```

Du måste göra samma ändring någon annanstans i ditt program också. Bläddra till botten av din kod och ändra raden som ställer in avbrottsutlösaren till:

```python
left_button.irq(trigger = Pin.IRQ_RISING, handler = button_handler)
```

Lägg till ytterligare en rad under den för att ställa in en avbrottsutlösare på din nya knapp också:

```python
right_button.irq(trigger = Pin.IRQ_RISING, handler = button_handler)
```

Ditt program ska nu se ut så här:

```python
from machine import Pin, utime
import urandom

tryckt = False
led = Pin(15, Pin.OUT)
left_button = Pin(14, Pin.IN, Pin.PULL_DOWN) 
right_button = Pin(16, Pin.IN, Pin.PULL_DOWN)

def button_handler(pin):
    globalt tryckt
    if not tryckt:
        tryckt = True
        timer_reaction = utime.ticks_diff(utime.ticks_ms(), timer_start)
        print("Din reaktionstid var " + str(timer_reaction) + " millisekunder!")

led.on()
utime.sleep(urandom.uniform(5, 10))
led.off()
timer_start = utime.ticks_ms()
right_button.irq(trigger = Pin.IRQ_RISING, handler = button_handler) 
left_button.irq(trigger = Pin.IRQ_RISING, handler = button_handler)
```

{% hint style="info" %}
**AVBRYT OCH HANTERARE**\
Varje avbrott du skapar behöver en hanterare, men en enda hanterare kan hantera så många avbrott som du vill. I detta fall programmet har du två avbrott som båda går till samma hanterare - vilket betyder att oavsett vilket avbrott som utlöser, kör de samma koda. Ett annat program kan ha två hanterare som låter var och en avbryt kör olika kod - allt beror på vad du behöver din program att göra.
{% endhint %}

Klicka på **Run**, vänta tills lysdioden slocknar och tryck sedan på vänster knapp för att se att spelet fungerar på samma sätt som tidigare och skriver ut din reaktionstid till konsolen. Klicka på **Run** igen, men den här gången när lysdioden släcks trycker du på högerknappen: spelet kommer att fungera på samma sätt och din reaktionstid skrivs ut som vanligt.

För att göra spelet lite mer spännande kan du låta det rapportera om vilken av de två spelarna som var först med att trycka på knappen. Gå tillbaka till toppen av ditt program, precis nedanför där du ställer in lysdioden och de två knapparna och lägg till följande:

```python
snabbaste_knappen = None
```

Detta ställer in en ny variabel, snabbaste\_knappen, och anger dess initiala värde till Ingen - eftersom ingen knapp har tryckts ännu. Gå sedan till botten av din knapphanterare och ta bort de två raderna som hanterar timern och utskriften - ersätt dem sedan med:

```python
global snabbaste_knappen
snabbaste_knappen = pin
```

Kom ihåg att dessa rader måste indragas med **TAB **så att MicroPython vet att de är en del av funktionen. Dessa två rader gör att din funktion kan ändra, snarare än att bara läsa, den **snabbaste\_knappen **variabeln och ställa in den för att innehålla detaljerna för pinnen som utlöste avbrottet - samma detaljer som ditt spel tryckt på konsolen tidigare i kapitlet, inklusive utlösningspinnens nummer.

Gå nu till botten av ditt program och lägg till dessa två nya rader:

```python
while snabbaste_knappen is None :
    utime.sleep(1)
```

Detta skapar en loop, men det är inte en oändlig loop: här har du sagt till MicroPython att köra koden i loopen endast när variabeln **snabbaste\_knappen **fortfarande är noll - värdet den initierades med i början av programmet. I själva verket pausar detta programmets huvudtråd till avbrottshanteraren ändrar värdet på variabeln. Om ingen av spelarna trycker på en knapp pausar programmet helt enkelt.

Slutligen behöver du ett sätt att avgöra vilken spelare som vann - och gratulera dem. Skriv följande längst ner i programmet och se till att du tar bort den fyrkantiga strecksatsen som Thony har skapat åt dig på första raden-dessa rader utgör inte en del av loopen:

```python
if snabbaste_knappen är vänster_knapp:
    print("Vänster spelare vinner!")
elif snabbaste_knappen är höger_knapp:
    print("Rätt spelare vinner!")
```

De första raden sätter upp ett 'om' villkorat som ser ut för att se om **snabbaste\_knappen **variabeln är **vänster\_knapp **- vilket innebär att IRQ utlöstes av knappen vänster. Om så är fallet kommer det att skriva ut ett meddelande-med raden nedan inryckt med fyra mellanslag så att MicroPython vet att det bara ska köra det om villkoret är True-gratulera vänster spelare, vars knapp är ansluten till GP14.

Nästa rad, som inte ska indragas, förlänger villkoret som ett "elif" - kort för "annars om", ett sätt att säga "om det första villkoret inte var True, kontrollera detta villkorligt nästa". Den här gången ser det ut om variabeln **snabbaste\_knappen **är **höger\_knapp **och skriver i så fall ut ett meddelande som gratulerar den högra spelaren, vars knapp är ansluten till GP16.

Ditt färdiga program ska se ut så här:

```python
from machine import Pin, utime
import urandom

tryckt = False
led = Pin(15, Pin.OUT)
left_button = Pin(14, Pin.IN, Pin.PULL_DOWN)
right_button = Pin(16, Pin.IN, Pin.PULL_DOWN)
snabbaste_knappenen = None

def button_handler(pin):
    globalt tryckt
    if not tryckt:
        tryckt = True
        global snabbaste_knappen
        snabbaste_knappen = pin

led.on()
utime.sleep(urandom.uniform(5, 10))
led.off()
timer_start = utime.ticks_ms()
left_button.irq(trigger = Pin.IRQ_RISING, handler = button_handler) 
right_button.irq(trigger = Pin.IRQ_RISING, handler = button_handler)

while snabbaste_knappen är Ingen :
    utime.sleep(1)

if snabbaste_knappen är vänster_knapp:
    print("Vänster spelare vinner!")
elif snabbaste_knappen är höger_knapp:
    print("Rätt spelare vinner!")
```

Tryck på Run-knappen och vänta tills lysdioden slocknar-men tryck inte på någon av knapparna ännu. Du kommer att se att konsolen förblir tomt och inte tar tillbaka >>> prompten; det beror på att huvudtråden fortfarande körs och sitter i loopen du skapade.

Tryck nu på vänster knapp, ansluten till pin GP14. Du kommer att se ett meddelande som gratulerar dig tryckt till konsolen - din vänstra hand var vinnaren! \
Klicka på **Run** igen och försök att trycka på högerknappen när lysdioden slocknar: du kommer att se ett annat meddelande tryckt, den här gången gratulerar din högra hand. \
Klicka på **Run** igen, den här gången med ett finger på varje knapp: tryck på dem båda samtidigt och se om din högra eller vänstra hand är snabbare!

Nu när du har skapat ett spel för två spelare kan du bjuda in dina vänner att spela med och se vem av er som har de snabbaste reaktionstiderna!

{% hint style="success" %}
**UTMANING: TIDNINGAR**\
Kan du ändra meddelandena som skrivs ut?\
Kan du lägga till en tredje knapp, så att tre personer kan spela samtidigt?\
Finns det en övre gräns för hur många knappar kan du lägga till?\
Kan du lägga till timern igen ditt program, så det berättar den vinnande spelaren hur snabbt deras reaktion var tiden?
{% endhint %}
