// Highlight all code blocks
function highlightFunctions() {

	// Get all code blocks inside ".highlight code"
	const blocks = Array.from(document.querySelectorAll(".highlight code"));

    // Iterate through all code blocks
    blocks.forEach((root) => {
        console.log("Found code block");

        // Get all code elements
		const codeElements = Array.from(root.getElementsByTagName('span'));

        // Iterate through all code elements
        codeElements.forEach(element => {
            console.log("Found code element", element.innerText);

            // Search for ending in period "."
            if (element.className.includes("p") && element.innerText.endsWith(".")) {
                console.log("Found period", element.innerText);

                // Is there a next element?
                if (element.nextElementSibling) {
                    console.log("Found next element", element.nextElementSibling.innerText);

                    // Check to see if next element has a class "n"
                    if (element.nextElementSibling.className.includes("n")) {
                        console.log("Found next element", element.nextElementSibling.innerText);

                        // Add class "f"
                        element.nextElementSibling.classList.add("f");
                    }
                }
            }
        });
    });
}

// When the page has finished loading, highlight all code blocks
document$.subscribe(() => {
    highlightFunctions();
});