# Rymd-shooter

## Skjuta dubbelskott uppåt

Vi har ett rymdskepp som vi kan styra med piltangenterna. Vi kan skjuta skott med tangenten Z.

Koden ser ut såhär:

```lua
FUNCTION _INIT()
    -- Initiera variabler
    X=60
    BX=200
    BY=200
    BSPEED=0
END

FUNCTION _UPDATE()
    -- Uppdatera variabler
    IF BTN(0) THEN
        X=X-1
    END
    IF BTN(1) THEN
        X=X+1
    END
    IF BTNP(5) THEN
        BSPEED=2
        BX=X
        BY=120
    END
    BY=BY-BSPEED
END

FUNCTION _DRAW()
    CLS(0)
    -- Rita rymdskepp
    SPR(1,X,120)
    -- Rita skott
    SPR(2,BX,BY)
END
```

