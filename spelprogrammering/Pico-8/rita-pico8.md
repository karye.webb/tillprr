# Rita i Pico-8

Pico-8 är en retroinspirerad spelkonsol och utvecklingsmiljö. I Pico-8 kan du skapa egna spel och grafik. I det här exemplet ska vi rita en enkel bild i Pico-8.

## Rita sprites

* Tryck på `Esc` för att öppna Pico-8 menyn.
* Välj `Sprite Editor` för att öppna sprite editorn.
* Rita en enkel bild med hjälp av musen.

![alt text](<../../.gitbook/assets/Skärmbild 2024-01-15 110203.png>)

* För att rita ut spriten i spelet, gå till kodfliken.
* Skriv följande kod för att rita ut spriten i spelet.

```lua
CLS()
SPR(1, 60, 60)
```

Spriten ritas ut på koordinaterna 60, 60.
Så här ser koordinatsystemet ut i Pico-8:

![alt text](../../.gitbook/assets/image-24.png)

* Tryck på `Ctrl` + `R` för att återgå till spelet.

![alt text](<../../.gitbook/assets/Skärmbild 2024-01-15 131526.png>)

## Cheat sheet

![alt text](../../.gitbook/assets/image-25.png)