# RPG Maker VX Ace Lite

![](../../.gitbook/assets/image.png)

RPG Maker VX Ace Lite är ett program för att skapa RPG-spel. Det är en gratisversion av RPG Maker VX Ace. Det är enkelt att komma igång med och det finns många guider på nätet. Det är ett bra sätt att lära sig grunderna i programmering och spelutveckling.

## Installation

* Installera **Steam** på datorn
* Installera gratisversionen **RPG Maker VX Ace Lite**

{% embed url="steam://install/224280" %}

