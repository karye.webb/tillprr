# RPG Maker VX Ace Lite

I den här tutorialen kommer vi att arbeta med händelser (events). "Events" är saker som händer i spelet, som att en karaktär pratar, en dörr öppnas eller en fiende dyker upp.

## En figur som pratar

Vi börjar med att skapa en figur som pratar. Följ stegen nedan:

1. Välj **Events** från verktygsfältet.
2. Klicka på kartan där du vill placera figuren, högerklicka och välj **New Event**.
3. Under **Graphic**, välj en bild för figuren genom att klicka på den lilla rutan.
4. I **Event Commands**, välj **Show Text** och skriv in texten som figuren ska säga: "Hej! jag heter Palbo".
5. Klicka på **OK** för att spara händelsen.

Starta spelet, gå fram till figuren och tryck på **Enter** för att prata med figuren.

## Fråga med svarsalternativ

Nu ska vi skapa ett nytt textmeddelande som frågar om vi vill gå in i huset. Följ stegen nedan:

1. Skapa en ny händelse genom att upprepa steg 1-2 från tidigare övning.
2. I **Event Commands**, välj **Show Text** och skriv "Vill du gå in i huset?".
3. Lägg till ett nytt kommando genom att klicka på **Show Choices** och skriv alternativen "Ja" och "Nej".

### Agera på svaren

Vi ska nu bestämma vad som ska hända beroende på spelarens svar.

1. Under alternativet "When [Ja]" i **Show Choices**, välj **Show Text** och skriv "Du gick in i huset".
2. Under alternativet "When [Nej]" kan du välja att inte göra något eller lägga till en annan text, till exempel "Du stannade utanför".

Klicka på **OK** för att spara händelsen.

## Uppgifter

### Uppgift 1: En dörr som öppnas och stängs

Skapa en dörr som kan öppnas och stängas av spelaren.

1. Skapa en ny händelse och välj en dörrbild som grafik.
2. I **Event Commands**, välj **Show Text** och skriv "Vill du öppna dörren?".
3. Lägg till **Show Choices** med alternativen "Ja" och "Nej".
4. Under "When [Ja]", lägg till **Show Text** med "Du öppnade dörren" och välj **Control Self Switch** för att aktivera Self Switch A.
5. Skapa en ny sida i händelsen och ställ in Self Switch A som en förutsättning. Ändra grafiken till en öppen dörr och lägg till ett kommando för att visa texten "Dörren är nu öppen".

### Uppgift 2: En skattkista med belöning

Skapa en skattkista som spelaren kan öppna för att få en belöning.

1. Skapa en ny händelse och välj en skattkista som grafik.
2. I **Event Commands**, välj **Show Text** och skriv "Vill du öppna skattkistan?".
3. Lägg till **Show Choices** med alternativen "Ja" och "Nej".
4. Under "When [Ja]", lägg till **Change Items** för att ge spelaren en belöning, till exempel en potion.
5. Lägg till **Show Text** med "Du fick en potion!" och välj **Control Self Switch** för att aktivera Self Switch A.
6. Skapa en ny sida i händelsen, ställ in Self Switch A som en förutsättning och ändra grafiken till en öppen skattkista.

### Uppgift 3: En NPC som ger en uppdrag

Skapa en NPC som ger spelaren ett uppdrag.

1. Skapa en ny händelse och välj en NPC-bild som grafik.
2. I **Event Commands**, välj **Show Text** och skriv "Hej äventyrare! Kan du hitta min förlorade amulett?".
3. Lägg till **Show Choices** med alternativen "Ja" och "Nej".
4. Under "When [Ja]", lägg till **Show Text** med "Tack! Kom tillbaka när du har hittat den." och aktivera en switch, till exempel "UppdragPåbörjat".
5. Skapa en ny händelse någon annanstans på kartan som representerar den förlorade amuletten.
6. När spelaren interagerar med amuletten, aktivera en switch, till exempel "AmulettHittad".
7. Skapa en ny sida i NPC-händelsen som kontrollerar "AmulettHittad" och lägg till ett kommando för att ge spelaren en belöning när de återvänder med amuletten.

### Uppgift 4: En fälla som skadar spelaren

Skapa en fälla som skadar spelaren när de går över den.

1. Skapa en ny händelse och välj en fälla som grafik.
2. I **Event Commands**, välj **Change HP** och minska spelarens HP med ett visst belopp.
3. Lägg till **Show Text** med "Aj! Du gick på en fälla och förlorade 10 HP.".
4. Eventuellt kan du lägga till ett ljudeffektkommando för att göra fällan mer dramatisk.

### Uppgift 5: En magisk portal

Skapa en magisk portal som teleportera spelaren till en annan plats på kartan.

1. Skapa en ny händelse och välj en portal som grafik.
2. I **Event Commands**, välj **Transfer Player** och ange den nya platsen på kartan där spelaren ska teleporteras.
3. Lägg till **Show Text** med "Du steg in i portalen och teleporterades till en ny plats.".
4. Eventuellt kan du lägga till en ljudeffekt eller en animation för att visa teleportationen.

Självklart! Här kommer fem förslag på mer omfattande uppgifter som inkluderar fler steg och en berättelse.

### Uppgift 1: En räddningsuppdrag

#### Del 1: Introduktion

1. Skapa en NPC som berättar för spelaren att deras vän har blivit tillfångatagen av banditer.
2. Välj **Show Text** och skriv "Hjälp! Min vän har blivit tillfångatagen av banditer i skogen. Kan du rädda honom?".
3. Lägg till **Show Choices** med alternativen "Ja" och "Nej".
4. Under "When [Ja]", lägg till **Show Text** med "Tack! Du hittar deras läger i skogen." och aktivera en switch, till exempel "RäddningsuppdragPåbörjat".

#### Del 2: Skog och banditläger

1. Skapa en ny karta som representerar skogen och lägg till en händelse som representerar banditlägret.
2. Skapa flera banditkaraktärer som spelaren måste besegra för att nå den tillfångatagna vännen.
3. Använd **Battle Processing** för att skapa strider med banditerna.

#### Del 3: Rädda vännen

1. Skapa en händelse i banditlägret som representerar den tillfångatagna vännen.
2. När spelaren interagerar med vännen, välj **Show Text** och skriv "Tack för att du räddade mig!".
3. Aktivera en switch, till exempel "VänRäddad".

#### Del 4: Återvänd till NPC

1. Återvänd till den ursprungliga NPC och skapa en ny sida som kontrollerar switchen "VänRäddad".
2. Välj **Show Text** och skriv "Tack för att du räddade min vän! Här är en belöning.".
3. Ge spelaren en belöning, till exempel guld eller ett föremål.

### Uppgift 2: En försvunnen skatt

#### Del 1: Introduktion

1. Skapa en NPC som berättar för spelaren om en försvunnen skatt.
2. Välj **Show Text** och skriv "Det sägs att en skatt är gömd i de gamla ruinerna. Vill du leta efter den?".
3. Lägg till **Show Choices** med alternativen "Ja" och "Nej".
4. Under "When [Ja]", lägg till **Show Text** med "Var försiktig, det sägs att ruinerna är förbannade." och aktivera en switch, till exempel "SkattjaktPåbörjad".

#### Del 2: Ruinerna

1. Skapa en ny karta som representerar de gamla ruinerna.
2. Lägg till fällor och fiender som spelaren måste undvika eller besegra.

#### Del 3: Pussel

1. Skapa ett pussel som spelaren måste lösa för att komma vidare, till exempel att flytta stenar i rätt ordning.
2. Använd **Move Route** och **Control Variables** för att skapa pusslet.

#### Del 4: Hitta skatten

1. Skapa en händelse som representerar skatten.
2. När spelaren interagerar med skatten, välj **Show Text** och skriv "Du hittade den försvunna skatten!".
3. Ge spelaren en värdefull belöning och aktivera en switch, till exempel "SkattHittad".

#### Del 5: Återvänd till NPC

1. Återvänd till den ursprungliga NPC och skapa en ny sida som kontrollerar switchen "SkattHittad".
2. Välj **Show Text** och skriv "Tack för att du hittade skatten! Här är en belöning.".
3. Ge spelaren ytterligare en belöning.

### Uppgift 3: Ett mysterium i byn

#### Del 1: Introduktion

1. Skapa en NPC som berättar för spelaren att något mystiskt händer i byn.
2. Välj **Show Text** och skriv "Något märkligt händer i byn på nätterna. Kan du undersöka det?".
3. Lägg till **Show Choices** med alternativen "Ja" och "Nej".
4. Under "When [Ja]", lägg till **Show Text** med "Tack! Kom tillbaka till mig med vad du hittar." och aktivera en switch, till exempel "MysteriumPåbörjat".

#### Del 2: Undersök byn

1. Skapa händelser runt om i byn som spelaren kan interagera med för att samla ledtrådar.
2. Använd **Control Variables** för att hålla reda på antalet ledtrådar som samlats in.

#### Del 3: Upptäck sanningen

1. När spelaren har samlat tillräckligt med ledtrådar (kontrollera variabeln), aktivera en ny händelse som avslöjar vad som händer i byn.
2. Skapa en strid eller en dialog där spelaren konfronterar skurken.

#### Del 4: Rapportera tillbaka

1. Återvänd till den ursprungliga NPC och skapa en ny sida som kontrollerar variabeln för antalet ledtrådar.
2. Välj **Show Text** och skriv "Tack för att du löste mysteriet! Här är en belöning.".
3. Ge spelaren en belöning.

### Uppgift 4: Ett handelsuppdrag

#### Del 1: Introduktion

1. Skapa en handelsman-NPC som ber spelaren att leverera varor till en annan by.
2. Välj **Show Text** och skriv "Kan du leverera dessa varor till byn i norr?".
3. Lägg till **Show Choices** med alternativen "Ja" och "Nej".
4. Under "When [Ja]", lägg till **Show Text** med "Tack! Ge dem till handelsmannen där." och aktivera en switch, till exempel "HandelsUppdragPåbörjat".

#### Del 2: Res till den andra byn

1. Skapa en ny karta som representerar den andra byn.
2. Lägg till fiender eller hinder på vägen som spelaren måste övervinna.

#### Del 3: Leverera varorna

1. Skapa en handelsman-NPC i den andra byn som tar emot varorna.
2. Välj **Show Text** och skriv "Tack för leveransen! Här är en belöning.".
3. Aktivera en switch, till exempel "VarorLevererade".

#### Del 4: Återvänd till ursprungshandelsmannen

1. Återvänd till den ursprungliga handelsmannen och skapa en ny sida som kontrollerar switchen "VarorLevererade".
2. Välj **Show Text** och skriv "Tack för att du levererade varorna! Här är en extra belöning.".
3. Ge spelaren ytterligare en belöning.

### Uppgift 5: Ett svärdsäventyr

#### Del 1: Introduktion

1. Skapa en NPC som berättar för spelaren om ett legendariskt svärd gömt i en farlig grotta.
2. Välj **Show Text** och skriv "Det sägs att ett legendariskt svärd är gömt i grottan i bergen. Vill du leta efter det?".
3. Lägg till **Show Choices** med alternativen "Ja" och "Nej".
4. Under "When [Ja]", lägg till **Show Text** med "Var försiktig, grottan är full av faror." och aktivera en switch, till exempel "SvärdsÄventyrPåbörjat".

#### Del 2: Utforska grottan

1. Skapa en ny karta som representerar grottan.
2. Lägg till fiender och fällor som spelaren måste övervinna.
3. Använd **Battle Processing** för att skapa strider med starka fiender.

#### Del 3: Hitta svärdet

1. Skapa en händelse som representerar det legendariska svärdet.
2. När spelaren interagerar med svärdet, välj **Show Text** och skriv "Du hittade det legendariska svärdet!".
3. Ge spelaren svärdet som ett föremål och aktivera en switch, till exempel "SvärdHittat".

#### Del 4: Återvänd till NPC

1. Återvänd till den ursprungliga NPC och skapa en ny sida som kontrollerar switchen "SvärdHittat".
2. Välj **Show Text** och skriv "Otroligt! Du hittade det legendariska svärdet! Här är en belöning för ditt mod.".
3. Ge spelaren en extra belöning, till exempel guld eller en annan kraftfull föremål.
